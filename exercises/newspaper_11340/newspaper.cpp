#include <iostream>
#include <map>

int main (){

    unsigned int n, k, m, max_nc, cost_tmp;
    char tmp;
    double cents;
    std::map<char,int> v;
    std::map<char,int>::iterator it;

    scanf("%d%*c", &n);
   
    while(n--){
        cents = 0;
        scanf("%d%*c", &k);

        while(k--) {
            scanf("%c", &tmp); 
            scanf("%d%*c", &v[tmp]);
        }

        scanf("%d%*c", &m);
        max_nc = m*10000; 

        while(max_nc-- && m){
            scanf("%c", &tmp);
            cents += (it = v.find(tmp)) != 
                        v.end() ? it->second : 0;
            if(tmp == '\n') m--;
        } 
    
        printf("%.2f$\n",cents/100);
        v.clear();
    }

	return 0;
}
