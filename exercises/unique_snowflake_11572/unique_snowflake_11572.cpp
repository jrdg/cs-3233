#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>
#include <stack>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

/**
 * Titre : Unique snowflake 11572
 *
 * url : https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2619
 *
 * EXPLICATION :
 *
 * Si on a une sequence S :
 *
 * S = {1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10}
 *      0  1  2  3  4  5  6  7  8  9  10
 *
 * - A chaque iteration de `S` nous associons dans
 *   une map `M` la valeur `S[i]` a `i` tel que
 *   `M[S[i]]` = `i`. 
 *
 * - Si la cle ne se trouve pas deja dans la map 
 *   alors ca signifie que ce nombre n'etait pas 
 *   deja present dans la sequence et qu'il na 
 *   donc pas de repetition jusqu'a date pour ce 
 *   nombre donc on incremente `C` et on fais 
 *   l'asscociation comme decrit plus haut. 
 *
 * - Si par contre la cle etait deja dans la map 
 *   M ca signifie que ce nombre etait PEUT ETRE 
 *   deja dans la sequence.
 *
 * - Pour en etre sur nous devons regarder si
 *   la valeur courrante de M[S[i]] est plus 
 *   grande ou egal a `LR` (l'indice de 
 *   commencement de cette sequence). Si c'est le
 *   cas alors ca signifie que cette sequence est
 *   terminer.
 *
 * - Nous regardons alors si le nombre d'element
 *   de cette sequence est plus grand que l'ancien
 *   max si c'est le cas on fait le swap sinon non.
 *
 * - Ont met a jour `C` en lui attribuant la valeur
 *   de l'indice courante moin M[S[i]] (ca nous 
 *   donne le nombre d'element de la nouvelle 
 *   sequence a partir de la derniere repetition du
 *   nombre repeter sans avoir a tout reiterer a 
 *   partir de la.). 
 *
 * - Nous mettons a jour `LR` (le nouveau indice 
 *   de depart de la prochaine sequence).
 *
 * EXPLICATION GRAPHIQUE :
 *
 * Variable de depart :
 *
 * S (sequence complete) = {1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10}
 *                          0  1  2  3  4  5  6  7  8  9  10
 *
 * C (Compteur d'element = 0
 *    dans la sous 
 *    sequence courannte)
 *
 * MAX (NB d'element     = 0
 *      de la sous 
 *      sequence la plus 
 *      grande)
 *
 * LR (Indice de         = 0 
 *     commencement de
 *     la sequence
 *     courante)
 *
 * Puisqu'il y a 10 element dans la sequence 
 * complete il y a 10 iteration.
 * 
 * AU COMMENCEMENT :
 *
 * C   = 0
 * MAX = 0
 * LR  = 0
 * M   = {}
 *
 * ------------------------------------------
 *
 * ITERATION 1
 *
 * C   = 0
 * MAX = 0
 * LR  = 0
 * M   = {}
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^
 *  S[i]
 *   LR
 *
 * Resultat de l'iteration :
 *
 * S[i] = 1 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * -------------------------------------------
 *
 * ITERATION 2
 * 
 * C   = 1
 * MAX = 0
 * LR  = 0
 * M   = [1] ->  0
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^  ^
 *  LR S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 2 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * -------------------------------------------
 *
 * ITERATION 3
 * 
 * C   = 2
 * MAX = 0
 * LR  = 0
 * M   = {
 *      [1]  -> 0
 *      [2]  -> 1
 *      [3]  ->
 *      [4]  ->
 *      [5]  ->
 *      [6]  ->
 *      [7]  ->
 *      [8]  ->
 *      [9]  ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^     ^
 *  LR    S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 3 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * -------------------------------------------
 *
 * ITERATION 4
 * 
 * C   = 3
 * MAX = 0
 * LR  = 0
 * M   = {
 *      [1] -> 0
 *      [2] -> 1
 *      [3] -> 2
 *      [4] -> 3
 *      [5] ->
 *      [6] ->
 *      [7] ->
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^        ^
 *  LR       S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 4 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * -------------------------------------------
 *
 * ITERATION 5
 * 
 * C   = 4
 * MAX = 0
 * LR  = 0
 * M   = {
 *      [1] -> 0
 *      [2] -> 1
 *      [3] -> 2
 *      [4] -> 3
 *      [5] ->
 *      [6] ->
 *      [7] ->
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^           ^
 *  LR          S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 5 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * --------------------------------------------
 *
 * ITERATION 6
 * 
 * C   = 5
 * MAX = 0
 * LR  = 0
 * M   = {
 *      [1] ->  0
 *      [2] -> 1
 *      [3] -> 2
 *      [4] -> 3
 *      [5] -> 4
 *      [6] ->
 *      [7] ->
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^              ^
 *  LR             S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 6 n'est pas dans la map M donc on
 * l'ajoute et on incremente C de 1.
 *
 * --------------------------------------------
 *
 * ITERATION 7
 * 
 * C   = 6
 * MAX = 0
 * LR  = 0
 * M   = {
 *      [1] ->  0
 *      [2] -> 1
 *      [3] -> 2
 *      [4] -> 3
 *      [5] -> 4
 *      [6] -> 5
 *      [7] -> 
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *   ^                 ^
 *  LR                S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 4 est deja dans la map M donc on
 * la sequence ce termine ici. 
 * 
 * On met a jour le MAX : MAX = max(MAX, C) = 
 *                        max(0,6) = 6
 *
 * On calcule le nouveau C a partir du dernier
 * indice de la derniere occurence de S[i].
 * Donnc puisque 4 est deja dans la map nous regardons
 * sa valeur M[4] = 3 et i = 6 donc i - M[4] = 6 - 3 = 3
 * Donnc le nouveau C = 3.
 *
 * On met a jour LR car maintenant la sequence ne commence
 * plus a 0 mais a l'indice 4.
 *
 * Et puis on met a jour M[4] = i;
 *
 * ---------------------------------------------
 *
 * ITERATION 8
 * 
 * C   = 3
 * MAX = 6
 * LR  = 4
 * M   = {
 *      [1] ->  0
 *      [2] -> 1
 *      [3] -> 2
 *      [4] -> 6
 *      [5] -> 4
 *      [6] -> 5
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *               ^        ^
 *               LR      S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 3 est deja dans la map M mais 
 * il est avant LR donc on ne fait qu'incremente
 * C. On met a jour M[3] = i = 7.
 *
 * ----------------------------------------------
 *
 * ITERATION 9
 * 
 * C   = 4
 * MAX = 6
 * LR  = 4
 * M   = {
 *      [1] ->  0
 *      [2] -> 1
 *      [3] -> 7
 *      [4] -> 6
 *      [5] -> 4
 *      [6] -> 5
 *      [8] ->
 *      [9] ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *               ^           ^
 *               LR         S[i]
 *
 * Resultat de l'iteration :
 * 
 * S[i] = 2 est deja dans la map M mais 
 * il est avant LR donc on ne fait qu'incremente
 * C. On met a jour M[2] = i = 8.
 *
 *
 * ----------------------------------------------
 *
 
 * ITERATION 10
 * 
 * C   = 5
 * MAX = 6
 * LR  = 4
 * M   = {
 *      [1]  ->  0
 *      [2]  ->  8
 *      [3]  ->  7
 *      [4]  ->  6
 *      [5]  ->  4
 *      [6]  ->  5
 *      [8]  ->
 *      [9]  ->
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *               ^              ^
 *               LR            S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 8 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * ---------------------------------------------
 *
 * ITERATION 10
 * 
 * C   = 6
 * MAX = 6
 * LR  = 4
 * M   = {
 *      [1]  ->  0
 *      [2]  ->  8
 *      [3]  ->  7
 *      [4]  ->  6
 *      [5]  ->  4
 *      [6]  ->  5
 *      [8]  ->  9
 *      [10] ->
 * }
 *
 *   0  1  2  3  4  5  6  7  8  9  10
 * { 1, 2, 3, 4, 5, 6, 4, 3, 2, 8, 10 }
 *               ^                 ^
 *               LR               S[i]
 *
 * Resultat de l'iteration :
 *
 * S[i] = 10 n'est pas dans la map M donc on
 * l'ajoute et on incremennte C de 1.
 *
 * --------------------------------------------
 *
 *  Output :
 *
 *  On affiche max(max,c) = max(6,7) = 7
 *
 *
 *
 */
int main (){
    int n, ns, t, max, fi, c, lr, tns;
    std::map<int,int>::iterator it;

    //On scan le nombre de teste a evaluer
    scanf("%d", &n);


    // On itere tous les teste
    while(n--) {

        std::map<int,int> map;

        /**
         * On initialise le compte de snowflake
         * a 0.
         */
        max = 0;
        fi = 0;
        c = 0;
        lr = 0;

        /**
         * on scan le nombre de snowflake pour
         * ce test
         */
        scanf("%d", &ns);

        tns = ns;

        /**
         * on itere et scan tous les snowflakes
         */
        while(ns--){

            /**
             * On scan le int qui represente ce 
             * snowflake
             */
            scanf("%d", &t);
           
            /**
             * si les case qui reste son moin
             * nnombreuse que max ca ne sert
             * a rien de continuer car c'est 
             * impossible de depasser max.
             */ 
            if(tns - lr <= max) continue;

            /**
             * Regarde si le snowflake
             * presentement iterer est deja
             * dans la sequence si oui on
             * agit en consequence sinon
             * on continue d'incrementer
             * le nombre de snowflake d'affiler
             * qui ne sont pas les memes.
             */ 
            if( (it = map.find(t)) != map.end() && it->second >= lr) {
                max = std::max( max, c );
                c = fi - it->second;
                lr = fi + 1 - ( fi - it->second );
            }else{
                c++;
            }

            /**
             * associe dans la map ce snowflake
             * a cet indidce.
             */
            map[t] = fi++;
        }

        /**
         * On affiche la sequence qui a un 
         * maximum de nombre qui ne se repete
         * pas.
         */
        printf("%d\n",std::max(max, c));
    }

    return 0;
}

