#include <iostream>
#include <algorithm>
#include <map>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){
    int n, ns, t, max, fi, c, lr, tns;
    std::map<int,int>::iterator it;
    std::map<int,int> map;
    scanf("%d", &n);
    while(n--) {
        max = 0;
        fi = 0;
        c = 0;
        lr = 0;
        scanf("%d", &ns);
        tns = ns;
        while(ns--){
            scanf("%d", &t);
            if(tns - lr <= max) continue;
            if( (it = map.find(t)) != map.end() && it->second >= lr) {
                max = std::max( max, c );
                c = fi - it->second;
                lr = fi + 1 - ( fi - it->second );
            }else{
                c++;
            }
            map[t] = fi++;
        }
        printf("%d\n",std::max(max, c));
        map.clear();
    }

    return 0;
}

