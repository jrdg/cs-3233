#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>

#define ui unsigned int
#define ull unsigned long long
#define ul unsigned long
#define ll long long
#define st size_t
#define us unsigned short
#define MAX 1000001

/**
 * EXPLICATION :
 *
 * L'idee general est que pour chaque index de l'
 * interval donne nous testons si ce bit est deja
 * setter a 1 si c'est le cas alors nnous arreton
 * car ca veu dire qu'il a un conflit. Sinon nous
 * continuons jusqu'a la limite de l'innterval
 * - 1. Nous n'avons pas besoin de tester le 
 * dernier car il est menntionner dans le
 * probleme que les intervales sont ouvert et 
 * que donnc la derniere et la premiere case peu 
 * deja etre setter a 1. Donc si nous ne testons
 * pas et ne settonns jamais le dernier bit d'un
 * interval a 1, le premier bit de la prochaine
 * interval si elle commence sur ce meme dernier
 * bit ne sera pas considerer comme deja setter 
 * et cx'est ce qu'on veu.
 *
 * EXEMPLE :
 *
 * Interval 1 : ]2,5[
 *
 * 0 0 1 1 1 0 0
 * 0 1 2 3 4 5 6
 *
 * Interval 2 ]5,6[
 *
 * 0 0 1 1 1 1 1
 * 0 1 2 3 4 5 6
 *
 * Donc comme demontrer le dernier bit na pas
 * a etre tester et setter.
 * 
 */
int main (){
    int n, m, s, e, it, f, ts, te;

    scanf("%d%d", &n, &m);

    while(n != 0 || m != 0){

        std::bitset<MAX> b;
        f = 0;

        while(n--){
            scanf("%d%d", &s, &e);
            if(!f) {
                for(st i = s ; i < e ; i++) {
                    if(b.test(i)) { f = 1 ; ;break; }
                    else { b.set(i); };
                }
            }
        }   

        while(m--){
            scanf("%d%d%d", &s, &e, &it);
            if(!f) {
                while(!f && s < MAX - 1) {
                    for(st i = s ; i < e ; i ++) {
                        if(b.test(i)) { f = 1 ; break; }
                        else {b.set(i) ;}
                    }
                    s += it;
                    if(e < MAX) e += it;
                    if(e >= MAX) e = MAX - 1;
                }
            }
        }

        f ? printf("CONFLICT\n") :
            printf("NO CONFLICT\n");   

        scanf("%d%d", &n, &m);
    }

    return 0;
}

