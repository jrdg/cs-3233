#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

/*
 *
 * TITRE : UVA 1203 - Argus
 *
 * URL : https://uva.onlinejudge.org/index.php?
 *	     option=com_onlinejudge&Itemid=8&page=
 *       show_problem&problem=3644
 *
 * EXPLICATION :
 *
 * - Il nous aient demander de lister les K premieres
 *   requete entrer dans la machine argus. 
 *  
 * - Chaque requete se voient attribuer un 
 *   q_num (numero d'identification). Ainsi 
 *   qu'un entier representant un interval
 *   de temps.
 *
 * - Ensuite les requete son executer a chaque interval
 *   de temps qui leur sont attribuer.
 * 
 * - Si >= 2 requetes sont executer a un moment donne en meme temps,
 *   alors la requete avec le plus petit id est executer avant.
 *
 * EXEMPLE :
 *
 * Le principe utiliser par l'algorithme est d'utiliser une priority_queue qui prend
 * une struct definit selon les attributs suivant :
 *
 * struct Pair {
 *
 *     ival   : valeur de interval de temps
 * 	   q_num  : Numero d'identification
 *     t_time : Trackeur de temps ( va ajouter la valeur ival 
 *							    a chaque fois que cette requete
 *							    sera executer. )
 * }
 *
 * Maintenant on utilise une priority-queue sur laquelle il faut redefinir la methode 
 * de comparaison utiliser pour trier la queue.
 *
 * La definition est la suivante :
 * 
 *     Si les deux requetes on le meme t_time  
 *         retourner la requete ayant le plus petit id
 *     retourner la requete ayant le plus petit t_time
 *
 * Algorithme :
 * 
 * 0. Inserer tous les elements (requetes) dans la priority_queue. 
 * 1. Creer une boucle qui iterer K fois :
 *     2. retirer l'element sur le dessu de la priority_queue.
 *     3. afficher son id car c'est ce qui est demander
 *     4. Incrementer le T_TIME de l'element retirer en (2.) par son IVAL
 *     5. pusher l'element a nouveau dans la queue.
 *
 * EXEMPLE VISUEL :
 *
 * Sois l'ensemble R representent les differentes requetes entrer dans Argus.
 * 
 * Les requetes son representer sous forme de 2-uplet noter t ou t[0] est q_num(id) et
 * ou t[1] est l'interval de temps.
 *
 * R = {
 *     (1, 100),
 *     (2, 75),
 * }
 * 
 * k = 2
 *
 * LISTE :
 *        2
 *
 * Ensuite la p_queue a changer : 
 *
 * p_queue = {                  p_queue = {
 *     (75, 2, 75 + 75),            (100, 1, 100),
 *     (100,1,200),                 (75, 2, 150),
 * }                            }
 * 
 *
 * donc on continue on prend affiche le id du premier element : 
 * 
 * LISTE :
 *        2
 *        1
 *
 * Ensuite la p_queue a changer : 
 *
 * p_queue = {                  p_queue = {
 *     (100, 1, 200 + 200),         (75, 2, 225),
 *     (75,2,225),                  (100, 1, 400),
 * }                            }
 *
 * FIN 
 */ 

struct p {
	int ival;
	int q_num;
	int t_time;
};

struct Compare_time { 
    bool operator()(struct p const& p1, struct  p const& p2) 
    {
		if(p1.t_time == p2.t_time)
			return p1.q_num > p2.q_num;
		return p1.t_time > p2.t_time; 
    } 
};

int main (){
	std::priority_queue<struct p, std::vector<struct p>, Compare_time> q;
	std::vector<int> tracker;
	int k, q_num, interval;
	struct p pair;
	char next[3];

	while(scanf("Register %d %d\n", &q_num, &interval)){
		pair = {interval, q_num, interval};
		q.push(pair);
	}
	scanf("#\n%d", &k);
	while(k--){
		pair = q.top(); q.pop();
		printf("%d\n", pair.q_num);
		pair.t_time += pair.ival;
		q.push(pair);
	}

	return 0;
}

