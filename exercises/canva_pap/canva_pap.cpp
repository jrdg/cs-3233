#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int main (){

	int nb_client_trouver = 13;
	double taux_rappel = 0.7;
	double taux_closing = 0.6;
	double avg_client_rapporte = 1500;

	std::cin >> nb_client_trouver;

	printf("Profit total par semaine : %f\n",nb_client_trouver * taux_rappel * taux_closing * avg_client_rapporte);
	printf("Profit total par anne    : %f\n",nb_client_trouver * taux_rappel * taux_closing * avg_client_rapporte * 52);
	printf("Profit jordan par semaine : %f\n",nb_client_trouver * taux_rappel * taux_closing * avg_client_rapporte * 0.22);
	printf("Profit jordan par anne    : %f\n",nb_client_trouver * taux_rappel * taux_closing * avg_client_rapporte * 52 * 0.22);

	return 0;
}

