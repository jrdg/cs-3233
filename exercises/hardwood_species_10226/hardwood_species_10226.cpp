#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

/**
 * Dans ce probleme il faut creer une map puis
 * ajouter dans la map chaque arbre puis
 * inrementer son nombre de 1 a chaque fois qu'on
 * renncontre a nouveau cet sorte d'arbre.
 * Ensuite on itere la map avec un iterateur
 * qui lui s'assurera d'iterer la map
 * en ordre alphabetique des key. 
 */
int main (){
    std::map<std::string,int>::iterator it;
    std::string line;
    int n; 
    double c;
    std::map<std::string,int> map;

    /**
     * On scan le nombre de test
     */
    scanf("%d\n", &n);

    /**
     * On evalue chaque teste
     */
    while(n--){

        /*
         * On set le nombre d'arbre a 0
         */
        c = 0;

        /*
         * On iterare chaque ligen de stdin tant
         * et ausis lonngtemps qu'on ne reconntre
         * pas une ligne vide.
         */    
        while(getline(std::cin, line) && line != "") {
            
            /**
             * On incnrement l'arbe voulu et 
             * on le creer si necessaire
             */
            map[line]++;

            /**
             * On increment le nombre d'arbre
             */
            c++;
        }

        /**
         * On itere la map en affichant
         * le pourcentage de cet arbre
         */
        it = map.begin();
        while(it != map.end()){
            printf("%s %.4f\n",&(it->first[0]), (it->second * 100 / c) );
            it++;
        }

        if(n > 0) printf("\n");
    }

    return 0;
}

