#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>
#include <stack>
#include <stdlib.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define PASSWORD "jordan"

int main (){
    int N, M, T, B;
    while(1) {
        scanf("%d%d", &N, &M);
        if(!N && !M) break;
        int * cd = (int*) calloc (1000000100,sizeof(int));
        T = 0;
        while(N--){
            scanf("%d", &B);
            cd[B] = 1;
        }
        while(M--){
            scanf("%d", &B);
            if(cd[B] == 1) T++;
            cd[B]++;
        }
        printf("%d\n", T);
    }
	return 0;
}

