#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

/*
 *
 * TITRE : 10954 - ADD ALL 
 *
 * URL : https://uva.onlinejudge.org/index.php?
 *		 option=com_onlinejudge&Itemid=8&category=
 *		 24&page=show_problem&problem=1895
 *
 * ALGORITHME :
 *
 *  L'algoritme ci-dessous marche que pour une seul sequence de nombre.
 *  Donc pour ce numero une boucle est creer avant l'algorithme ci-dessous
 *  pour reinitialiser la priority queue ainsi que les variable R et T.
 *
 * 1. Remplir une priority_queue avec les nombre 
 *    en multipliant chacun d'eux par -1 (C'est pour
 *    que la PQ soit trier en ordre croissant)
 *
 * 2. Creer une boucle avec la condition de sortie :
 *    tant que le nombre d'element de la priority_queue 
 *    est > 1.
 *
 *    1. Affecter a une variable T la somme des deux
 *       premiers elements de la priority queue.
 *	
 *    2. supprimer ces deux elements de la priority_queue
 * 
 *    3. Pusher la variable T dans la priority queue
 *
 *    4. Incrementer une variable R (resultat final) par 
 *       la valeur de la variable T.
 *
 * 3. Afficher la variable R en la multipliant par -1.
 *
 *
 *
 * EXEMPLE VISUEL :
 *
 * - Soit S une sequence de nombre.
 * - Soit PQ une priority_queue qui se voit remplir par les valeurs de S.
 * - Soit N = |S|
 * - Soit R une variable representant le cout total des additions.
 * - Soit T une variable contenant la somme des deux premiers element 
 *   sur le top de PQ.
 *
 * S = { 9, 5, 4, 8, 2, 4, 5, 6, 3, 5, 4 }
 *
 * 1. Remplir la priority queue avec chaqun des elements de la sequence S 
 *    en multipliant par -1 chaque nombre :
 *
 *     PQ = { -2, -3, -4, -4, -5, -5, -5, -6, -9, -48 }
 *
 * 2. Tant que |PQ| > 1 : 
 *   
 *     Premiere iteration :
 *
 *	   PQ = { -2, -3, -4, -4, -5, -5, -5, -6, -9, -48 }
 *     R = 0
 *      
 *     T = -2 -3 = -5
 *     R += T
 *     PQ.pop(-2 et -3 a leur indice respective)
 *     PQ.push(T);
 *     
 *	   Deuxieme iteration :
 *
 *	   PQ = { -4, -4, -5, -5, -5, -5, -6, -9, -48 }
 *     R = -5
 *      
 *     T = -4 -4 = -8
 *     R += T
 *     PQ.pop(-4 et -4 a leur indice respective)
 *     PQ.push(T);
 *
 *	   troisieme iteration :
 *
 *	   PQ = { -5, -5, -5, -5, -6, ,-8, -9, -48 }
 *     R = -13
 *      
 *     T = -5 -5 = -10
 *     R += T
 *     PQ.pop(-5 et -5 a leur indice respective)
 *     PQ.push(T);
 *
 *	   troisieme iteration :
 *
 *	   PQ = { -5, -5, -6, -8, -9,-10, -48 }
 *     R = -23
 *      
 *     T = -5 -5 = -10
 *     R += T
 *     PQ.pop(-5 et -5 a leur indice respective)
 *     PQ.push(T);
 *
 *	   quatrieme iteration :
 *
 *	   PQ = { -6, -8, -9, -10, -10 -48 }
 *     R = -33
 *      
 *     T = -6 -8 = -14
 *     R += T
 *     PQ.pop(-6 et -8 a leur indice respective)
 *     PQ.push(T);
 *
 *	   cinquieme iteration :
 *
 *	   PQ = { -9, -10, -10, -14, -48 }
 *     R = -47
 *      
 *     T = -9 -10 = -19
 *     R += T
 *     PQ.pop(-9 et -10 a leur indice respective)
 *     PQ.push(T);
 *
 *	   cinquieme iteration :
 *
 *	   PQ = { -10, -14, -19, -48 }
 *     R = -66
 *      
 *     T = -10 -14 = -24
 *     R += T
 *     PQ.pop(-10 et -14 a leur indice respective)
 *     PQ.push(T);
 *
 *	   sixieme iteration :
 *
 *	   PQ = { -19, -24, -48 }
 *     R = -90
 *      
 *     T = -19 -24 = -43
 *     R += T
 *     PQ.pop(-19 et -24 a leur indice respective)
 *     PQ.push(T);
 *
 *	   septieme iteration :
 *
 *	   PQ = { -43, -48 }
 *     R = -133
 *      
 *     T = -43 -48 = -91
 *     R += T
 *     PQ.pop(-43 et -48 a leur indice respective)
 *     PQ.push(T);
 *
 *	   huitieme iteration :
 *
 *	   PQ = { -91 }
 *     R = -224
 *		
 *     Puisque PQ = { -91 } la boucle ce fini car |PQ| = 1
 *	      
 *     Donc le resultat de R est afficher soit la valeur R * -1 = -224 * -1 = 224.
 *
 * FIN 
 */


int main (){
	int N, R, T;
	std::priority_queue<int> q;
	while(scanf("%d\n", &N) && N != 0) {
		R = 0; T = 0;
		while(N--) {
			scanf("%d", &T);
			q.push(T * -1);
		}
		while(q.size() > 1){
			T = q.top();q.pop();
			T += q.top();q.pop();
			R += T;
			q.push(T);
		}
		printf("%d\n", R * -1);
		q.pop();
	}
	return 0;
}

