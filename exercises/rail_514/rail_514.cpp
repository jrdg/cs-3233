#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int main (){
	int N, coaches , t, stop;
	std::stack<int> station;	
	std::queue<int> order;

	while(scanf("%d\n", &N) && N != 0) {
		stop = 0;
		while(true){
		    coaches = 1;
			t = 1;
			for(size_t i = 0 ; i < N ; i++){
				scanf("%d", &t);
				if(t == 0){
					stop = 1;
					break;
				}
				order.push(t);
			}
			fgetc(stdin);
			if(stop){
				stop = 0;
				break;
			}
			while(!order.empty() && coaches <= N) {
				do{
					station.push(coaches++);
				} while(station.top() != order.front() && coaches <= N);
				while(!order.empty() && !station.empty() &&
				  	  station.top() == order.front()){
					station.pop();
					order.pop();
				}
			}
			if(order.empty()) printf("Yes\n");
			else printf("No\n");
			while(!station.empty()) station.pop();	
			while(!order.empty()) order.pop();
		}
		printf("\n");
	}


	return 0;
}

