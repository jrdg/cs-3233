#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int main (){
	std::list<char> l;
	std::list<char> tl;
	std::list<char>::iterator it;
	char t;
	int dir = 1;

	while(scanf("%c", &t) != EOF){
		if(t == '\n'){
			it = l.begin();
			l.splice(it, tl);
			while(!l.empty()){
				printf("%c", l.front());
				l.pop_front();
			}
			printf("\n");
			tl.clear();
		}else{
			if(t == '[') {
				if(!tl.empty()){
					it = l.begin();
					l.splice(it, tl);
					tl.clear();
				}
				dir = 0;
			}
			else if (t == ']') {
				dir = 1;
			}
			else{
				if(dir == 0){
					tl.push_back(t);
				}else{
					if(!tl.empty()){
						it = l.begin();
						l.splice(it, tl);
						tl.clear();
					}
					l.push_back(t);
				}
			}
		}
	}	

	return 0;
}
