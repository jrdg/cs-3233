#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>


/**
 * TTIRE : Spiral Tap - 10920
 *
 * URL : https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1861
 *
 * EXPLICATION :
 *
 * L'exemple suivant est donnee avec l'input 5 25
 * donc une grille de 5x5 dans laquelle nous
 * recherchons une la coordonne (x,y) du nombre
 * 18.
 *
 * 1. Donc voici ci-dessous la grille de 5x5 :
 *   
 *    05| 13 12 11 10 25
 *    04| 14 03 02 09 24
 *    03| 15 04 01 08 23
 *    02| 16 05 06 07 22
 *    01| 17 18 19 20 21
 *       ---------------
 *        01 02 03 04 05
 *
 * 2. Ci-dessous voici la meme grille avec le
 *    trajet jusqu'au nombre 25 en suivant un
 *    chemin en spiral :
 *
 *    | = Deplacement vertical
 *    - = deplacement horizontal
 *    
 *    05| 13--12--11--10  25
 *      |  |           |   |
 *    04| 14  03--02  09  24
 *      |  |   |   |   |   |
 *    03| 15  04  01  08  23
 *      |  |   |       |   |
 *    02| 16  05--06--07  22
 *      |  |               |
 *    01| 17--18--19--20--21
 *       ---------------------
 *        01  02  03  04  05
 *
 *
 * 3. Ci-dessous une representation lineaire 
 *    non graphique des deplacement vertical
 *    et horizontal du graphique ci-dessus :
 *
 *    -- = deplacement horizontal = O
 *     | = deplacemennt vertical  = X
 *    01 = Bond 0 bond null       = N
 *    
 *                        N  X  O  X  X  0  0  X  X  X  O  O  O  X  X  X  X  O  O  O  O  X  X  X  X
 *    Numero des bonds : 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
 *
 *    L'analyse de la sequence ci-dessus nous
 *    ammemne a constater quelle suit un pattern
 *    bien precis.
 *
 *    (Nous pouvons remplacer les X par SZ et les
 *    O par 1 puisque pour chaque de ligne il
 *    faut dans un tableau une dimension simuler
 *    en tableau 2 dimension faire - ou + le nombre
 *    d'element par ligne et -1 ou + 1 pour changer de
 *    case a l'horizontal.)
 *
 *    En effet, la sequence commence par un
 *    deplacement vertical vers le haut puis
 *    un deplacement horizontal vers la gauche
 *    puis 2 deplacement vertical vers le bas
 *    puis 2 deplacement horizontal vers la
 *    gauche et ainsi de suite en ajoutant
 *    toujours pour chaque sequence
 *    vertical/horizontal + 1 deplacement 
 *    horizontal et vertical.
 *
 *    Donc voici la meme representation lineaire
 *    definit ci-dessus mais cette fois-ci en
 *    prenant a compte la direction de chaque
 *    deplacement ainsi qu'en ajoutant un
 *    separateur entre chaque sequence 
 *    vertical/horizontal.
 *
 *    Deplacement vertical vers le haut     = -X
 *    Deplacement vertical vers le bas      = +X
 *    Deplacement horizontal vers la gauche = -O
 *    Deplacement horizontal vers la droite = +O
 *
 *                               N  | -X -O | +X +X +0 +0 | -X -X -X -O -O -O | +X +X +X +X +O +O +O +O | -X -X -X -X -X -O -O -O -O -O
 *    Numero des bonds :         00 | 01 02 | 03 04 05 06 | 07 08 09 10 11 12 | 13 14 15 16 17 18 19 20 | 21 22 23 24 25 26 27 28 29 30
 *    Deplacement par sequence          2         4                 6                     8                           10
 *    Numero                            1         2                 3                     4                            5
 *
 * 4. Maintenant puisque chaque sequence est 
 *    compose d'une distribution egale de
 *    deplacement vertical/horizontal et que
 *    le nombre de deplacement de chaque sequence
 *    est augmenter de 2 de sequence en sequence
 *    nous pouvons utiliser le theorem 1.
 *
 *    pour savoir a quel sequence appartient le nombre donner 
 *    il suffit d'utiliser le theorem 1.3.
 *
 *    Donc puisque nous recherchons 25 :
 *
 *    round( sqrt( 25 ) ) = 5 
 *    
 *    Ce nombre concorde aussi avec le nombre
 *    total de deplacement vertical/horizontal
 *    de cette sequence qui ont chacun 5
 *    deplacement et ce sera tous le temps vrai
 *    nous pouvons le voir en regardant les autre
 *    sequences. En effet chaque sequence a une
 *    seul racine carrer entiere qui correspond
 *    aussi a son numero de sequence.
 *
 *    sequences :
 *
 *      1 : 01
 *      2 : 04 
 *      3 : 09
 *      4 : 16
 *      5 : 25
 *      
 *    
 * 5. Ensuite il faut trouver la distribution
 *    des -+X/-+O pour pouvoir calculer l'indice
 *    voulu. Pour ce faire il faut calculer
 *    le maximum de bond de la sequence
 *    representer par le numero trouver en (4).
 *
 *    nset = numero de la sequence
 *
 *    Nombre maximum de bonds = nset * ( nset + 1 ) = 5 * ( 5 + 1 ) = 30
 *
 *    Ensuite avec ce nombre nous pouvons 
 *    calculer le nombre de X present dans le 
 *    total de bond puisque nous savons deja que 
 *    nous recherhons les coordonne du nombre 25
 *    et que le nombre de bond qui separe 1 de 25
 *    et definit par 25 - 1 = 24 bonds. Nous
 *    savons aussi que dans chaque sequence les
 *    deplacement verticaux (X) sont tous
 *    effectue avant les deplacemennt horizontaux
 *    et que le nombre maximum de bond de la sequence
 *    - son numero de sequence est egal au dernier
 *    X de la sequence car rappelons qu'une sequence
 *    a le meme nombre de deplacemennt verticaux/horizontaux
 *    par sequence.
 *
 *    Donc si :
 *
 *    MSET = maximum de bond de la sequence                                  = 30
 *    NSET = numero de la sequence                                           = 5
 *    B    = nombre de bonnd a effectuer pour arriver a 25 = p - 1 = 25 - 1 = 24
 *
 *    Alors le nombre de deplacement verticaux =
 *          
 *    DCX = Dernier Case du deplacement X de la sequence = mset - nset = 30 - 5 = 25
 *
 *    Ensuite il ne nous reste plus qu'a faire la difference
 *    entre DCX et B pour trouver le nombre a soustraire du
 *    nombre de deplacement verticaux.
 *
 *    R = DCX - B = 25 - 24 = 1
 *    
 *    Et donc nous pouvons calculer le nombre
 *    total de deplacement verticaux :
 *      
 *    Puisque le nombre de deplacement X/O de tous les sequence
 *    est egaux nous pouvons conclure que le maximum de deplacement
 *    X ou O peu etre au maximum : MSET / 2
 *
 *    Donc le nombre de deplacement vertical est :
 *
 *    NSZ = ( mset / 2 ) - R
 *
 *    Ensuite nous pouvons simplement soustraire a B
 *    NSZ pour trouver le nombre de deplacemennt horizontal.
 *
 *    N1 = B - NSZ
 *
 * 6. Tous se qu'il reste a faire pour pouvoir
 *    calculer de maniere precise l'indice final
 *    de notre case 25 c'est de trouver le nombre
 *    de deplacemennt vertical de chaque sens ainsi 
 *    que le nombre de deplacement horizontal
 *    de chaque sens.
 *
 *    Nous pouvons en analysant a nouveau notre
 *    pattern en (3) que :
 *
 *    1. les signes -+ s'inverse chacun leur tour
 *       de sequence en sequence.
 *
 *    2. La sequence representant le nombre de -X
 *       est une suite arithmetique de raison 2
 *       dont le premier terme est 1.
 *
 *    3. La sequence representant le nombre de +X
 *       est une suite arithmetique de raison 2
 *       dont le premier terme est 2.
 *    
 *    4. La sequence representant le nombre de -O
 *       est une suite arithmetique de raison 2
 *       dont le premier terme est 1.
 *
 *    5. La sequence representant le nombre de +O
 *       est une suite arithmetique de raison 2
 *       dont le premier terme est 2.
 *
 *    6. Si le numero de sequence trouver en (4)
 *       est impair alors la derniere sequence
 *       est compose de -X/-O si elle est pair
 *       +X/+O
 *
 *    Avec ces analyse nous pouvons facilement 
 *    calculer avec le theorem de la somme d'une
 *    suite arithmetique la distirbution des
 *    deplacement avec leur sens.
 *  
 *                               N  | -X -O | +X +X +0 +0 | -X -X -X -O -O -O | +X +X +X +X +O +O +O +O | -X -X -X -X -X -O -O -O -O -O
 *    Numero des bonds :         00 | 01 02 | 03 04 05 06 | 07 08 09 10 11 12 | 13 14 15 16 17 18 19 20 | 21 22 23 24 25 26 27 28 29 30
 *    Deplacement par sequence          2         4                 6                     8                           10
 *    Numero                            1         2                 3                     4                            5
 *
 *    Si NSET est impair :
 *
 *    pos_sz = ( ( 2 + nset - 1 ) / 2 ) * ( ( nset - 3 ) / 2 + 1 );
 *    neg_sz = nsz - pos_sz;
 *    pos_1 = ( ( 2 + nset - 1 ) / 2 ) * ( ( nset - 3 ) / 2 + 1 );
 *    neg_1 = n1 - pos_1; 
 *
 *    Si NSET est pair :
 *
 *    neg_sz = ( ( 1 + nset - 1) / 2 ) * ( ( nset - 1 ) / 2 + 1 );
 *    pos_sz = nsz - neg_sz;
 *    neg_1 = ( ( 1 + nset - 1) / 2 ) * ( ( nset - 1 ) / 2 + 1 );
 *    pos_1 = n1 - neg_1;
 *
 *    Ici NSET = 5
 *
 *    Nb +SZ = 6 
 *    Nb -SZ = 8
 *    Nb -1 = 4
 *    Nb +1 = 6
 *
 *    T = ( +SZ * 6 ) + ( -SZ * 8 ) + ( -1 * 4 ) + ( +1 * 6 )
 *
 * 7. On calcul le nombre d'indice que nous
 *    devons parcourir pour arriver a 25
 *
 *    La case numero 1 est au centre de la grille
 *    et son indice est calculer de la facon suivante :
 *
 *    SZ*SZ / 2
 *
 *    Et donc l'indice de notre case est :
 *
 *    FI = SZ * SZ / 2 + T
 *
 * 8. Nous pouvons maintenant calculer les coordonnees
 *    de la case P :
 *
 *    X = FI % NSET + 1
 *    Y = SZ - ( ( FI - X + 1 ) / SZ );
 *
 *    THEOREM 1 :
 *
 *    1.1 Si la racine carrer d'un nombre N nous donne
 *    un nombre entier alors le prochain nombre N
 *    pour lequel la racine carrer sera enntiere peu etre
 *    calculer de la facon suivante : N + (sqrt(N) * 2) + 1
 *
 *    1.2 calcul du nombre d'element entre une racine carre entiere 
 *    et la prochaine racine carre entier : (sqrt(N) * 2)
 * 
 *    1.3 Pour savoir vers quelle nombre un nombre elever a la puissance 2
 *    donnant un nombre entier est pret un nombre M ayant une racinen
 *    carrer non entiere : round( sqrt( M ) )
 *
 *    1.4 Pour savoir vers quelle nombre N ayant unen racine
 *    carrer entiere un nombre M aynt une racine carrer nnon entiere 
 *    est le plus pret : N = pow( round( sqrt( M ) ), 2 )
 *
 *
 */
int main (){

    long long pos_sz, neg_sz, pos_1, neg_1, p, sz, nset ,mset, r, nsz, n1, t, fi, x, y;

    scanf("%llu%llu", &sz, &p );

    while(sz != 0) {
        
        /** 
         * Calcul dans quelle numero de
         * sequence est p
         */
        nset = round( sqrt( p ) );

        /**
         * Calcul du maximum de bond
         * dans cette sequence 
         */
        mset = ( 1 + nset ) * nset;

        /**
         * Calcule du nombre de sz a supprimer
         * de la sequence.
         */
        r = ( ( mset - nset ) - ( p - 1 ) );

        /**
         * Calcule du nombre final de -+sz 
         */
        nsz = ( mset / 2 )  - (r > 0 ? r : 0);

        /**
         * Calcul du nombre de -+1
         */
        n1 = ( p - 1 ) - nsz;

        
        /**
         * Calcule du nombre total de -sz, +sz,
         *                            -1,  +1
         */
        if(nset % 2 == 1) {
            pos_sz = ( ( 2 + nset - 1 ) / 2 ) * ( ( nset - 3 ) / 2 + 1 );
            neg_sz = nsz - pos_sz;
            pos_1 = ( ( 2 + nset - 1 ) / 2 ) * ( ( nset - 3 ) / 2 + 1 );
            neg_1 = n1 - pos_1; 
        } else {
            neg_sz = ( ( 1 + nset - 1) / 2 ) * ( ( nset - 1 ) / 2 + 1 );
            pos_sz = nsz - neg_sz;
            neg_1 = ( ( 1 + nset - 1) / 2 ) * ( ( nset - 1 ) / 2 + 1 );
            pos_1 = n1 - neg_1; 
        } 

        /**
         * Calcul du total de bond a effectuer
         * a partir de l'indice de depart de la 
         * case 1
         */
        t = ( pos_sz * sz +
            ( -1 * sz * neg_sz ) ) +
            ( pos_1  + ( -1 * neg_1 ) );

        /**
         * Calcule de l'indice finale de la case
         * recherche
         */
        fi = (sz * sz / 2) + t;

        /**
         * Calcul final des coordonnees (x,y) de
         * la case recherche.
         */
        x = fi % sz + 1;
        y = sz - ( ( fi - x + 1 ) / sz );

        printf("Line = %lld, column = %lld.\n", y, x);
        scanf("%lld%lld", &sz, &p);
    }

    return 0;
}

