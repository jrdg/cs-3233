#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>
#include <set>
#include <stack>
#include <cmath>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){
    std::multiset<int> blue, green;
    std::multiset<int>::iterator ITG, ITB;
    std::stack<int> wb, wg;
    int N, SG, SB, B, TMP;
    scanf("%d", &N);
    while(N--){
        scanf("%d%d%d", &B, &SG, &SB);
        while(SG--){
            scanf("%d", &TMP);
            green.insert(TMP);
        }
        while(SB--){
            scanf("%d", &TMP);
            blue.insert(TMP);
        }
        while(!blue.empty() && !green.empty()){
            for(st i = 0 ; i < B ; i++){
                if(blue.empty() || 
                green.empty()) break;
                (ITB = blue.end())--;
                (ITG = green.end())--;
                if((TMP = *ITG - *ITB) < 0) 
                    wb.push(std::abs(TMP));
                else if(TMP > 0) wg.push(TMP);
                green.erase(ITG);
                blue.erase(ITB);
            }
            while(!wb.empty()){
                blue.insert(wb.top());
                wb.pop();
            }
            while(!wg.empty()){
                green.insert(wg.top());
                wg.pop();
            }
        }
        if(blue.empty() && green.empty()){
            printf("green and blue died\n");
        }else if(green.empty()){
            printf("blue wins\n");
        }else if(blue.empty()){
            printf("green wins\n");
        }
        for(std::multiset<int>::reverse_iterator i = blue.rbegin(); i != blue.rend() ; i++) {
            printf("%d\n", *i);
        }
        for(std::multiset<int>::reverse_iterator i = green.rbegin() ; i != green.rend() ; i++) {
            printf("%d\n", *i);
        }
        if(N > 0) printf("\n");
        blue.clear();
        green.clear();
    }

    return 0;
}

