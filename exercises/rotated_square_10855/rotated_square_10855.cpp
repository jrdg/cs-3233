#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>

unsigned int check_0(char NA[], 
              char na[], 
              size_t si, 
              size_t N, 
              size_t n) {
    
    int inc = N - n; 
    for(size_t row = 0 ; row < n ; row ++){
        for(size_t col = 0 ; col < n ; col++) {
            if(NA[si] == na[row * n + col]) {
                si++;
            }else return 0;
        }
        si += inc;
    }
    return 1;
}

unsigned int check_90(char NA[], 
              char na[], 
              size_t si, 
              size_t N, 
              size_t n) {
    int inc = N - n; 
    for(size_t col = 0 ; col < n ; col ++){
        for(size_t row = 1 ; row <= n ; row++) {
            if(NA[si] == na[ (n-row) * n + col]) {
                si++;
            }else return 0;
        }
        si += inc;
    }

    return 1;
}

unsigned int check_180(char NA[], 
              char na[], 
              size_t si, 
              size_t N, 
              size_t n) {
    
    int inc = N - n;
    for(int row = n-1 ; row >= 0 ; row--){
        for(int col = n-1 ; col >= 0 ; col--) {
            if(NA[si] == na[row * n + col]) {
                si++;
            }else return 0;
        }
        si += inc;
    }

    return 1;
}

unsigned int check_270(char NA[], 
              char na[], 
              size_t si, 
              size_t N, 
              size_t n) {
    int inc = N - n; 
    for(int col = n - 1 ; col >= 0 ; col--){
        for(int row = n ; row > 0 ; row--) {
            if(NA[si] == na[ (n-row) * n + col]) {
                si++;
            }else return 0;
        }
        si += inc;
    }
    return 1;
}

/**
 *
 * TITRE : 10855 - Rotated square
 * 
 * URL : https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1796
 */
int main (){
    unsigned long long int N, n;
    unsigned int rot0 = 0, rot90 = 0, rot180 = 0, rot270 = 0;
    size_t inc, powN, pown, max;
    char tmpc, NA[50000], na[50000];

    scanf("%llu%llu", &N, &n);
    
    while(N != 0){

        powN = N*N+N;
        pown = n*n+n;
        
        inc = 0;
        while(powN--){
            if((tmpc = getchar()) != '\n'){
                NA[inc++] = tmpc;
            }
        }

        inc = 0;
        while(pown--){
            if((tmpc = getchar()) != '\n'){
                na[inc++] = tmpc;
            }
        }
       
        
        powN = N*N;
        max = (N - n) * N + (N - n);
        int row = 0;
        for(size_t i = 0 ; i < powN && i <= max ; i++) {
            
            if(i > (N - n) + (N * row)) {
                row++;
                i += (n-1);
            }

            rot0   += check_0(NA, na , i, N, n);
            rot90  += check_90(NA, na , i, N, n);
            rot180 += check_180(NA, na , i, N, n);
            rot270 += check_270(NA, na , i, N, n);
        }
        
        printf("%u %u %u %u\n", rot0, rot90, 
                                    rot180, rot270);
        rot0 = 0; rot90 = 0; rot180 = 0; 
        rot270 = 0;

        scanf("%llu%llu", &N, &n);
    }

    return 0;


}

