#include <bits/stdc++.h>
#include <unistd.h>

enum Direction {
	ORTHOGONAL,
	DIAGONAL
};

struct Arete {
	unsigned int etiquette_depart;
	unsigned int etiquette_arrive;
	unsigned int est_visite;
	double ponderation;
	enum Direction direction;
};

struct Sommet {
	unsigned int etiquette;
	char type_case;
	unsigned int elevation;
	unsigned int visite;
	std::map<unsigned int, Arete> aretes_sortantes;
};


class Graph {
		std::map<unsigned int, Sommet> sommets;
		unsigned int porte_presente;
		unsigned int prochaine_etiquette;
		size_t nb_sommets;
	public:	
		Graph();
	
		size_t size();

		Sommet * sommet_a(
			unsigned int etiquette
		);

		unsigned int ajouter_sommet(
			char type_case,
			unsigned int elevation
		);

		unsigned int modifier_sommet(
			Sommet *& sommet
		);

		unsigned int supprimer_sommet(
			Sommet *& sommet
		);
		
		double calcule_ponderation(
			Sommet & depart,
			Sommet & arrive,
			enum Direction direction
		);

		void ajouter_arete(
			unsigned int etiquette_no_1,
			unsigned int etiquette_no_2,
			enum Direction direction
		);

		unsigned int modifier_arete(
			Arete *& arete
		);

		unsigned int supprimer_arete(
			Arete *& arete
		);

		unsigned int existe_une_relation_entre(
			Sommet & premier_sommet, 
			Sommet & deuxieme_sommet
		);

		void lister_sommets(
			 void
		 );

		void lister_aretes(
			void
		 );
	
		void lister_aretes_sommet(
			Sommet *& sommet
		);
		
		double dijkstra(
			Sommet * depart,
			Sommet * arrive
		);
};

