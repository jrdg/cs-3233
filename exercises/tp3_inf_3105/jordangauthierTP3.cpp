#include <bits/stdc++.h>
#include "Graph.h"

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

enum Erreur {
	PREMIERE_LIGNE = 0,
	LIGNE_TRESOR = 1,
	MATRICE = 2,
	AUCUNE_ERREUR = 3
};

struct Coordonne {
	int no_colonne;
	int no_ligne;
};

unsigned int premiere_ligne_ok(
	char * buffer,
	char * restant_de_ligne,
	int & largeur_x,
	int & hauteur_y
) {
	return sscanf(buffer, "%d %d%s", &largeur_x,
		   &hauteur_y, restant_de_ligne) == 2 && 
		   strlen(restant_de_ligne) == 0; 
}


void ajout_arete_bas_diagonale_gauche(
	Graph & graph,
	Sommet *& sommet,
	unsigned int largeur_x
) {
	graph.ajouter_arete(sommet->etiquette,
						sommet->etiquette + largeur_x - 1,
						DIAGONAL);
}

void ajout_arete_bas_diagonale_droite(
	Graph & graph,
	Sommet *& sommet,
	unsigned int largeur_x
) {
	graph.ajouter_arete(sommet->etiquette, 
						sommet->etiquette + largeur_x + 1,
					    DIAGONAL);
}

void ajout_arete_bas(
	Graph & graph,
	Sommet *& sommet,
	unsigned int largeur_x
) {
	graph.ajouter_arete(sommet->etiquette, 
						sommet->etiquette + largeur_x, 
						ORTHOGONAL);
}

void ajout_arete_devant(
	Graph & graph,
	Sommet *& sommet,
	unsigned int largeur_x
) {
	graph.ajouter_arete(sommet->etiquette, 
						sommet->etiquette + 1, 
						ORTHOGONAL);
}

void ajout_arete_cycle(
	Graph & graph,
	Sommet *& sommet,
	unsigned int largeur_x
) {
	graph.ajouter_arete(sommet->etiquette, 
						sommet->etiquette + largeur_x - 1, 
						ORTHOGONAL);
}

unsigned int sommet_est_premiere_case_ligne(
	unsigned int modx
) {
	return modx == 0;
}

unsigned int sommet_est_derniere_case_ligne(
	unsigned int modx,
	unsigned int largeur_x
) {
	return modx == largeur_x - 1;
}

unsigned int sommet_est_derniere_ligne(
	unsigned int etiquette,
	unsigned int largeur_x,
	unsigned int hauteur_y
) {
	return ( ( etiquette / largeur_x ) + 1 ) == hauteur_y;
}

void ajout_arete_cycle_diago_droite(
	Graph & graph,
	unsigned int etiquette,
	unsigned int largeur_x
) {
	graph.ajouter_arete(etiquette, 
				etiquette + largeur_x * 2 - 1, DIAGONAL);
}

void ajout_arete_cycle_diago_gauche(
	Graph & graph,
	unsigned int etiquette,
	unsigned int largeur_x
) {
	graph.ajouter_arete(etiquette, 
				etiquette + 1, DIAGONAL);
}

unsigned int sommet_est_sur_derniere_ligne(
	unsigned int etiquette,
	unsigned int largeur_x
) {
	return etiquette / largeur_x >= largeur_x - 1;
}

void ajout_aretes_premiere_case_ligne(
	Graph & graph,
	Sommet *& sommet,
	unsigned int etiquette,
	unsigned int largeur_x,
	unsigned int hauteur_y
) {
	if(!sommet_est_derniere_ligne(etiquette, largeur_x, hauteur_y)) { 
		ajout_arete_bas_diagonale_droite(graph, sommet, largeur_x);
		ajout_arete_bas(graph, sommet, largeur_x);
	}
	ajout_arete_devant(graph, sommet, largeur_x);
	ajout_arete_cycle(graph, sommet, largeur_x);
	if(!sommet_est_sur_derniere_ligne(etiquette, largeur_x)) {
		ajout_arete_cycle_diago_droite(graph, etiquette, largeur_x);
	}
}

void ajout_aretes_derniere_case_ligne(
	Graph & graph,
	Sommet *& sommet,
	unsigned int etiquette,
	unsigned int largeur_x,
	unsigned int hauteur_y

) {
	if(!sommet_est_derniere_ligne(etiquette, largeur_x, hauteur_y)) {
		ajout_arete_bas_diagonale_gauche(graph, sommet, largeur_x);
		ajout_arete_bas(graph, sommet, largeur_x);
	}
	if(!sommet_est_sur_derniere_ligne(etiquette, largeur_x)) {
		ajout_arete_cycle_diago_gauche(graph, etiquette, largeur_x);
	}
}

void ajout_aretes_case_central_ligne(
	Graph & graph,
	Sommet *& sommet,
	unsigned int etiquette,
	unsigned int largeur_x,
	unsigned int hauteur_y

) {
	if(!sommet_est_derniere_ligne(etiquette, largeur_x, hauteur_y)) { 
		ajout_arete_bas_diagonale_droite(graph, sommet, largeur_x);
		ajout_arete_bas_diagonale_gauche(graph,sommet, largeur_x);
		ajout_arete_bas(graph, sommet, largeur_x);
	} 
	ajout_arete_devant(graph, sommet, largeur_x);
}

void ajouter_aretes(
	Graph & graph,
	unsigned int etiquette,
	unsigned int largeur_x,
	unsigned int hauteur_y
) {
	Sommet * sommet = graph.sommet_a(etiquette);
	unsigned int modx = sommet->etiquette % largeur_x;
	if(sommet_est_premiere_case_ligne(modx)){
		ajout_aretes_premiere_case_ligne(graph, sommet, etiquette, 
										 largeur_x, hauteur_y);
	} else if(sommet_est_derniere_case_ligne(modx, largeur_x)) {
		ajout_aretes_derniere_case_ligne(graph, sommet, etiquette, 
										 largeur_x, hauteur_y);
	} else {
		ajout_aretes_case_central_ligne(graph, sommet, etiquette, 
										largeur_x, hauteur_y);
	}	
}

struct Chemin {
	int v1;
	int v2;
	int v3;
	double p1;
	double p2;
	double p3;
	double p4;
};

void compute(
	Graph & graph,
	Sommet * porte ,
	int v1, 
	int v2, 
	int v3,
	Sommet * t1,
	Sommet * t2,
	Sommet * t3, 
	double * result
) {
	double r_tmp[8];
	r_tmp[0] = graph.dijkstra(porte, t1);
	r_tmp[1] = v1;
	r_tmp[2] = graph.dijkstra(t1, t2);
	r_tmp[3] = v2;
	r_tmp[4] = graph.dijkstra(t2, t3); 
	r_tmp[5] = v3;
	r_tmp[6] = graph.dijkstra(t3, porte);
	r_tmp[7] = r_tmp[0] + r_tmp[2] + r_tmp[4] + r_tmp[6];
	if(r_tmp[7] < result[7] ) {
		for(size_t i = 0 ; i < 8 ; i++) {
			result[i] = r_tmp[i];
		}
	}
}

int main (){
	Graph graph;
	int largeur_x, hauteur_y, tmpd, modx, mody, tmpy;
	char buffer[10000], restant_de_ligne[10];
	enum Erreur err = AUCUNE_ERREUR;
	char tmpc;
	int tresor_tmp[2];
	Sommet * tmps = NULL;
	Sommet * tresor[3];
	Sommet * porte = NULL;
	if(fgets(buffer , 10000, stdin) != NULL &&
	   premiere_ligne_ok(buffer, restant_de_ligne, largeur_x, hauteur_y)) {
		tmpy = hauteur_y;
		while(tmpy-- && err == AUCUNE_ERREUR){
			for(st i = 0 ; i < largeur_x && err == AUCUNE_ERREUR ; i++) {
				std::cin >> tmpc >> tmpd;
				if(!graph.ajouter_sommet(tmpc, tmpd))
					err = MATRICE;
				if(tmpc == 'P'){
					if(porte == NULL)
						porte = graph.sommet_a(graph.size() - 1);
					else
						err = MATRICE;
				} 
			}
		}
		getchar();
		for(st etiquette = 0 ; etiquette < graph.size() ; etiquette++)
			ajouter_aretes(graph, etiquette, largeur_x, hauteur_y);

		for(st i = 0 ; i < 3 && err == AUCUNE_ERREUR ; i++) {
			if(fgets(buffer , 10000, stdin) == NULL     || 
			   sscanf(buffer, "%d %d%s", &tresor_tmp[0], 
			   &tresor_tmp[1], restant_de_ligne) != 2   || 
			   strlen(restant_de_ligne) != 0            || 
			   (tresor[i] = graph.sommet_a(tresor_tmp[1] * largeur_x + tresor_tmp[0])) == NULL) {
					err = LIGNE_TRESOR;
			}
		}
		if(fgets(buffer , 10000, stdin) != NULL){
			err = LIGNE_TRESOR;
		}
	} else{
		err = PREMIERE_LIGNE;
	}

	if(err == AUCUNE_ERREUR) {
		double result[8];
		for(st i = 0 ; i < 8 ; i++)
			result[i] = INFINITY;
		compute(graph, porte, 1, 2, 3,tresor[0], tresor[1], tresor[2], result);
		compute(graph, porte, 1, 3, 2,tresor[0], tresor[2], tresor[1], result);
		compute(graph, porte, 2, 1, 3,tresor[1], tresor[0], tresor[2], result);
		compute(graph, porte, 2, 3, 1,tresor[1], tresor[2], tresor[0], result);
		compute(graph, porte, 3, 1, 2,tresor[2], tresor[0], tresor[1], result);
		compute(graph, porte, 3, 2, 1,tresor[2], tresor[1], tresor[0], result);
		std::cout << "P -> " << "T" << result[1] << " : " << result[0] <<std::endl;	
		std::cout << "T" << result[1] << " -> " << "T" << result[3] << " : " << result[2] <<std::endl;
		std::cout << "T" << result[3] << " -> " << "T" << result[5] << " : " << result[4] <<std::endl;	
		std::cout << "T" << result[5] << " -> P " << " : " << result[6] <<std::endl;
		std::cout << "Total : " << result[7] << std::endl;
	}else{
		std::cout << err << std::endl;
	}
	
	return 0;
}
