/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include "Graph.h"


Graph::Graph(){
	prochaine_etiquette = 0;
	porte_presente = 0;
	nb_sommets = 0;
}

size_t Graph::size() {
	return this->nb_sommets;
}

Sommet * Graph::sommet_a(
	unsigned int etiquette
) {
	std::map<unsigned int, Sommet>::iterator it;
	return ((it = sommets.find(etiquette)) != sommets.end()) ?
										   &it->second : NULL;
}

unsigned int Graph::ajouter_sommet(
	char type_case,
	unsigned int elevation
) {
	unsigned int ajout = 0;
	if(elevation >= 1 && elevation <= 9 && 
	   type_case == 'N' || type_case == 'E' ||
       type_case == 'F' || type_case == 'R' ||
       (type_case == 'P' && porte_presente == 0 &&
	   (porte_presente = 1))) {
		sommets.insert(
			std::pair<unsigned int, Sommet>(
				prochaine_etiquette,
				{.etiquette = prochaine_etiquette,
			 	 .type_case = type_case,
				 .elevation = elevation,
			 	 .visite = 0}
			)
		);
		prochaine_etiquette++;
		nb_sommets++;
		ajout = 1;
	}
	return ajout;
}


unsigned int Graph::modifier_sommet(
	Sommet *& sommet
) {
	return 0;
}

unsigned int Graph::supprimer_sommet(
	Sommet *& sommet
) {
	return 0;
}

double Graph::calcule_ponderation(
	Sommet & depart,
	Sommet & arrive,
	enum Direction direction
) {
	double ponderation = INFINITY;
	int diff = 0;
	if(arrive.type_case != 'E' && 
	   abs((diff = arrive.elevation - depart.elevation)) < 2) {
		ponderation = 0.0;
		if(arrive.type_case == 'F') {
			ponderation += direction == Direction::ORTHOGONAL ? 2.0 : 2.8;			
		} else if(depart.type_case == 'R' && arrive.type_case == 'R') {
			ponderation += direction == Direction::ORTHOGONAL ? 0.5 : 0.7;	
		} else{
			ponderation += direction == Direction::ORTHOGONAL ? 1.0 : 1.4;
		}	
		if(diff == 1) {
			ponderation *= 2;
		}
	}
	return ponderation;
}

void Graph::ajouter_arete(
	unsigned int etiquette_no_1,
	unsigned int etiquette_no_2,	
	enum Direction direction
) {
	sommets[etiquette_no_1].aretes_sortantes.insert(
		std::pair<unsigned int, Arete>(
			etiquette_no_2,{
			.etiquette_depart = etiquette_no_1,
			.etiquette_arrive = etiquette_no_2,
			.ponderation = calcule_ponderation(
						       sommets[etiquette_no_1], 
							   sommets[etiquette_no_2], 
	 						   direction
						   ),
			.direction = direction
		})
	);
	sommets[etiquette_no_2].aretes_sortantes.insert(
		std::pair<unsigned int, Arete>(
			etiquette_no_1,{
			.etiquette_depart = etiquette_no_2,
			.etiquette_arrive = etiquette_no_1,
			.ponderation = calcule_ponderation(
						       sommets[etiquette_no_2], 
							   sommets[etiquette_no_1], 
							   direction
						   ),
			.direction = direction
		})
	);
}

unsigned int Graph::modifier_arete(
	Arete *& arete
) {
	return 0;
}

unsigned int Graph::supprimer_arete(
	Arete *& arete
) {
	return 0;
}

unsigned int Graph::existe_une_relation_entre(
	Sommet & premier_sommet, 
	Sommet & deuxieme_sommet
) {
	return 0;
}

void Graph::lister_sommets(
	 void
) {
	std::cout << sommets.size() << std::endl;
	std::cout << "-------------------------------------" << std::endl;
  	std::cout << "               SOMMETS               " << std::endl;
  	std::cout << "-------------------------------------" << std::endl;
  	for (std::map<unsigned int , Sommet>::iterator it = sommets.begin(); it != sommets.end(); ++it) {
    	std::cout << "Etiquette       : " << it->second.etiquette << '\n';
    	std::cout << "Type de la case : " << it->second.type_case << '\n';
    	std::cout << "Elevation       : " << it->second.elevation << '\n';
    	std::cout << "Visite          : " << it->second.visite << '\n';
		for (std::map<unsigned int, Arete>::iterator it2 = it->second.aretes_sortantes.begin(); it2 != it->second.aretes_sortantes.end(); ++it2)
    		std::cout << it->second.etiquette
					  << " --> "
					  << sommets[it2->second.etiquette_arrive].etiquette 
					  << " Ponderation : " 
					  << it2->second.ponderation  
					  << std::endl;
  		std::cout << "-------------------------------------" << std::endl;
	}
}

void Graph::lister_aretes(
	void
) {
	
}
	
void Graph::lister_aretes_sommet(
	Sommet *& sommet
) {

}


typedef std::pair<double, Sommet> iPair;

struct Comp{
    bool operator()(const iPair & a, const iPair & b){
        return a.first < b.first;
    }
};

double Graph::dijkstra(
	Sommet * depart,
	Sommet * arrive
) {
	double distance_min[nb_sommets];
	double distance;
	unsigned int parents[nb_sommets];
	Sommet sommet_courant;
	for(int i = 0 ; i < nb_sommets ; i++){
		distance_min[i] = INFINITY;
		parents[i] = -1;
	}
	distance_min[depart->etiquette] = 0;
	std::priority_queue< iPair, std::vector <iPair> , Comp > pq; 	
	pq.push(std::make_pair(0,*depart));
	while(!pq.empty()) {
		sommet_courant = pq.top().second;
		pq.pop();
		if(sommet_courant.etiquette == arrive->etiquette) continue;
		for (std::map<unsigned int, Arete>::iterator it = 
			 sommet_courant.aretes_sortantes.begin();
			 it != sommet_courant.aretes_sortantes.end(); it++) {
			distance = distance_min[sommet_courant.etiquette] + it->second.ponderation;
			if(distance < distance_min[it->second.etiquette_arrive]){
				distance_min[it->second.etiquette_arrive] = distance;
				parents[it->second.etiquette_arrive] = sommet_courant.etiquette;
				pq.push(std::make_pair(distance, sommets[it->second.etiquette_arrive]));
			}
		}
	}
	return distance_min[arrive->etiquette];
}
