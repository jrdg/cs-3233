#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <algorithm>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short

/**
 * A retenir :
 * 
 * La racine carrer d'un nombre N est la plus
 * grande valeur qu'un nombre A peu etre tel que 
 * A * B = N ou B >= A.
 *
 * Exemple :
 *
 * N = 36
 * sqrt( 36 ) = 6
 *
 * A | B
 *
 * 1 * 36 = 36
 * 2 * 18 = 36
 * 3 * 12 = 36
 * 4 * 9  = 36
 * 5      = nombre non entier
 * 6 * 6  = 36 (On voit que sqrt(36) = 6 est le plus grand nombre de la sequence avant la repetition des pairs.)
 * 7      = nombre non entier
 * 8      = nombre non entier
 * 9 * 4  = 36 (9 * 4 est deja present et A > B) 
 *
 */
int main (){

    int c, r, f, d, n, N, B, j = 0;
    std::vector<int> v;  

    scanf("%d", &n);

    while(j++ < n) {
        scanf("%d%d", &c, &r);

        printf( "Case #%d:", j );

        if(c == r){
            printf(" 0\n");
            continue;
        }

        N = c - r;
        B = sqrt( N );

        for(st A = 1 ; A <= B ; A++) {
            if ( N % A == 0 ) {
                if (N / A > r ) v.push_back( N / A );
                if( A > r && A != N / A) v.push_back( A );
            } 
        }

        std::sort( v.begin(), v.end() );

        for(st i = 0 ; i < v.size() ; i++) {
            printf( " %d", v.at(i) );
        }

        printf("\n");
        v.clear();
    }

    return 0;
}

