#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <math.h>

int main (){

    int N[9] = {1,2,3,4,5,6,7,8,9};
    int n = 3;

    FILE * deg0 = fopen ("0.txt" , "w+");
    FILE * deg90 = fopen ("90.txt" , "w+");
    FILE * deg180 = fopen ("180.txt" , "w+");
    FILE * deg270 = fopen ("270.txt" , "w+");

    if(deg0 != NULL && deg90 != NULL &&
            deg180 != NULL && deg270 != NULL) {
        for(size_t row = 0 ; row < n ; row++) {
            for(size_t col = 0 ; col < n ; col++) {
                fprintf(deg0,"%u ",N[n * row + col]);
                fprintf(deg90,"%u ",N[n * (n - 1 - col) + row]);
                fprintf(deg180,"%u ", N[(int)pow((double)n,2) - (n * row) - col - 1]);
                fprintf(deg270,"%u ", N[n * col + (n - 1) - row]);
            }
            fprintf(deg0,"\n");
            fprintf(deg90,"\n");
            fprintf(deg180,"\n");
            fprintf(deg270,"\n");
        }
        fclose(deg0);
        fclose(deg90);
        fclose(deg180);
        fclose(deg270);
        return 0;
    }

    fprintf(stderr, "ERROR : 1 or more files coun't have been created");
    return 1;
}

