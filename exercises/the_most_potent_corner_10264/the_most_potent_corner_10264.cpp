#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <algorithm>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short


/**
 *
 * A RETENIR :
 *
 * - Une nombre binaire ne contenant qu'un seul 
 *   bit setter a 1 est une puissance de deux.
 *
 * - Pour verifier qu'un nombre binaire est une 
 *   puissance de deux il faut tester la
 *   condition suivante : Si x est notre 
 *   nombre binaire alors (x & (x - 1)) = 0
 *   si cette condition est rempli alors le
 *   nombre est une puissance de deux.
 */
int main (){

    int n, a[1000000] = {0}, wa[1000000] = {0} , c, x, m;
    std::vector<std::vector<int>> map;  
    
    while(  scanf("%d", &n) != EOF ) {

        c = pow(2,n);
        m = 0;

        /**
         * On rempli le tableau (a)
         */
        for(st i = 0 ; i < c ; i++)
            scanf("%d", &a[i]); 

        /**
         * On boucle a travers tous les coins
         */
        for(int i = 0 ; i < c ; i++) {

            std::vector<int> v;
            map.push_back(v);

            /**
             * Pour chaque coin on itere chaque coin
             */
            for(int j = i + 1 ; j < c ; j++) {

                /**
                 * On fait un XOR sur le coinn numero
                 * i en binnaire et le coin numero j 
                 * en binnaire
                 */
                x = i ^ j;

                /**
                 * Deux coins son voisin s'ils ont un
                 * xor qui resulte en une puissance 
                 * de 2.
                 */
                if(i != j && !(x & (x-1))) {
                    
                    /**
                     * on ajoute le poid du coin i
                     * a la potency du coinn j et
                     * le poid du coin j a la potency
                     * du coin i.
                     */
                    wa[i] += a[j];
                    wa[j] += a[i];

                    /**
                     * On ajoute le mapping du coin j
                     * au coin i (pas besoinn de faire
                     * aussi l'inverse car on a seulement
                     * besoin de calculr la somme de ces
                     * 2 la plus tard alors si on fais linverse
                     * nous allons le mapper deux fois)
                     */
                    map.at(i).push_back(j);
                }
            }

        }

        for(st i = 0 ; i < map.size() ; i++){
            for(st j = 0 ; j < map.at(i).size(); j++){
                m = std::max(m, wa[i] + wa[map.at(i).at(j)]);
            }
            wa[i] = 0;
        }

        printf("%d\n", m);
        map.clear();
    }

    return 0;
}

