#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <algorithm>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short


/**
 *
 * A RETENIR :
 *
 * - Une nombre binaire ne contenant qu'un seul 
 *   bit setter a 1 est une puissance de deux.
 *
 * - Pour verifier qu'un nombre binaire est une 
 *   puissance de deux il faut tester la
 *   condition suivante : Si x est notre 
 *   nombre binaire alors (x & (x - 1)) = 0
 *   si cette condition est rempli alors le
 *   nombre est une puissance de deux.
 */
int main (){

    int n, a[1000000] = {0}, wa[1000000] = {0} , c, x, m;
    
    while(  scanf("%d", &n) != EOF ) {

        c = 2 << (n - 1);
        m = 0;

        for(st i = 0 ; i < c ; i++)
            scanf("%d", &a[i]); 

        for(int i = 0 ; i < c ; i++) {
            for (st j = 0 ; j < n ; j++) {
                wa[i] += a[ i ^ (1 << j) ];
            }
        }

        for(st i = 0 ; i < c ; i ++){
            for(st j = 0 ; j < n ; j++) {
                m = std::max(m, wa[i] + wa[i ^ (1 << j)] );
            }
            wa[i] = 0;
        }

        printf("%d\n", m);

    }

    return 0;
}

