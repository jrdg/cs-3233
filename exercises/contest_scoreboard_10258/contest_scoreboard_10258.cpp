#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int compare(int arr_i, int arr_j , int p_solved[], int t_track[]){
	if(p_solved[arr_i] == p_solved[arr_j]){
		if(t_track[arr_i] == t_track[arr_j])
			return arr_i < arr_j;
		return t_track[arr_i] < t_track[arr_j];
	}
	return p_solved[arr_i] > p_solved[arr_j];
}

void quicksort(int arr[], int p_solved[], int t_track[] , int start, int end) {
	if(start >= end) return;
	int pivot = start;
	int tmp;
	int i = start + 1;
	int j = end;
	while(i <= j) {
		if(compare(arr[i]-1,
                   arr[j]-1,
                   p_solved,
                   t_track) == 0){
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
		if(compare(arr[i]-1,
		           arr[pivot]-1,
                   p_solved,
                   t_track) == 1) i++;
		if(compare(arr[j]-1,
                   arr[pivot]-1,
                   p_solved,
                   t_track) == 0) j--;		
	}
	tmp = arr[j];
	arr[j] = arr[start];
	arr[start] = tmp;
	quicksort(arr, p_solved, t_track, start, j-1);
	quicksort(arr, p_solved, t_track, j+1, end);
}

int main (){
	int N, c, p, t, tmp;
	char l;
	scanf("%d\n", &N);
	int TN = N;
	while(N--) {
		int arr[100][9] = {{0},{0}};
		int final_contestant_time[100] = {0};
		int final_contestant_p_solved[100] = {0};
		int sort_arr[100] = {0};
		int already_in[100]= {0}; 
		int size = 0;
		if(N+1 != TN) printf("\n");
		while((tmp = getchar()) != '\n' && tmp != EOF) {
			ungetc(tmp, stdin);
			scanf("%d %d %d %c", &c, &p, &t, &l);
			getchar();
			if(arr[c-1][p-1] > -1){	
				if(l == 'I'){
					arr[c-1][p-1] += 20;
				} else if(l == 'C') {
					arr[c-1][p-1] += t;
					final_contestant_time[c-1] += arr[c-1][p-1];
					arr[c-1][p-1] = -1;
					final_contestant_p_solved[c-1]++;
				}
			}
			if(!already_in[c-1]){
				sort_arr[size++] = c;
				already_in[c-1] = 1;
			}
		}
		quicksort(sort_arr,
				  final_contestant_p_solved,
				  final_contestant_time, 
				  0,
				  size-1);	
		for(size_t i = 0 ; i < size ; i++){
			printf("%d %d %d\n", sort_arr[i], 
								 final_contestant_p_solved[sort_arr[i]-1],
								 final_contestant_time[sort_arr[i]-1]);
		}
	}
	return 0;
}

