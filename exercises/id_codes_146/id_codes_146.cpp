#include <iostream>
#include <string>
#include <string.h>
#include <bitset>
#include <vector>
#include <cmath>
#include <algorithm>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){

    char c[27];

    scanf( "%s\n", c );

    while( c[0] != '#' ) {
        if( std::next_permutation( c, c+strlen(c) ) ){
            printf( "%s\n", c );
        }else {
            printf( "No Successor\n" );
        }
        scanf( "%s\n", c );    
    }

    return 0;
}

