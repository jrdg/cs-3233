#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int main (){
	int N, t1, t2, stack, queue, p_queue;
	while(scanf("%d", &N) != EOF) {
		std::stack<int> s;
		std::queue<int> q;
		std::priority_queue<int> pq;

		stack = 1; p_queue = 1; queue = 1;
		while(N--){
			scanf("%d %d\n", &t1, &t2);
			if(t1 == 1){
				s.push(t2);
				q.push(t2);
				pq.push(t2);
			}else{
				if(queue) {
					if(q.empty() || q.front() != t2) queue = 0;
					else q.pop();
				}
				if(stack){
					if(s.empty() || s.top() != t2) stack = 0;
					else s.pop();
				}
				if(p_queue){
					if(pq.empty() || pq.top() != t2) p_queue = 0;
					else pq.pop();
				}
			}
		}	
		if(p_queue){
			if(stack || queue) printf("not sure\n");
			else printf("priority queue\n");
		}else if(stack){
			if(queue) printf("not sure\n"); 
			else printf("stack\n");
		}else if(queue){
			printf("queue\n");
		}else{
			printf("impossible\n");
		}
	} 
	return 0;
}

