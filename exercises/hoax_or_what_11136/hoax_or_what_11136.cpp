#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>
#include <stack>
#include <set>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){
    int N, K, B;
    ll T;
    std::multiset<int> u;
    std::multiset<int>::iterator M,m;
    while(1){                  
        scanf("%d", &N); 
        if(!N) break;
        T = 0;
        while(N--){
            scanf("%d", &K);
            while(K--){
                scanf("%d", &B);
                u.insert(B);
            };
            (M = u.end())--;
            m = u.begin();
            T += *M - *m;
            u.erase(M);
            u.erase(m);
        }
        printf("%lld\n", T);
        u.clear();
    }
    return 0;
}

