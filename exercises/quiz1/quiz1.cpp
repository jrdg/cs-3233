#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

int main (){
	
	int t[10];
		
	for(st i = 0 ; i < 10 ; i++){
		t[i] = 10 - i;
	}

	int *k = t + 3;

	std::cout << *k << std::endl;
	k++;
	std::cout << k[1] << std::endl;
	(*k)++;
	std::cout << k[0] << std::endl;
	*(k++);
	std::cout << *k << std::endl;

	return 0;
}

