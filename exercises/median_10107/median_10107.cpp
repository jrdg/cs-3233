#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <algorithm>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){

    int x, sz, s, a[10001];

    /**
     * On fill le array
     */
    while(scanf("%d", &a[sz]) != EOF) sz++; 
    
    /**
     * On itere les nombre 1 par 1
     * en prenant le temps de calculer
     * la mediane a chaque index pour le
     * tableau partant de 0 a cette index.
     */
    for(st i = 0 ; i < sz ; i++) {

        /**
         * On trie l'element au milieu du array
         */
        std::nth_element(a, a + (i / 2), a + i + 1);

        /**
         * On sauvegarde l'elemennt trier pour eviter
         * qu'une deuxieme appele de nth_element 
         * ne change la place de cette case trier.
         */
        s = a[i/2];

        /**
         * Si le nombre d'elemennt dans le array
         * est pair alors il faut faire un second
         * appelle de nth_element sur la case en 
         * avant de celle deja trier car un tabl
         * eau pair a 2 elemennt en son centre.
         */
        if( !((i + 1) % 2) ) {
            std::nth_element(a, a + (i / 2 + 1), a + i + 1);
            s = (s + a[i/2+1]) / 2;
        }

        /**
         * On affiche la medianne
         */
        printf("%d\n",s);
    }
    
    return 0;
}

