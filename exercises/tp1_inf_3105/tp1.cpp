#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Rectangle {
	std::pair<double,double> upper_left;
	std::pair<double,double> upper_right;
	std::pair<double,double> lower_left;
	std::pair<double,double> lower_right;
};

/**
	Structure contenant la coordonne (x,y)
	d'un rectangle avec le plus haut y du coter gauche.
	
	@param id    : numero unique d'identification du rectangle
    @param x     : coordonne x
	@param y     : coordonne y
	@param event : 0 pour entry 1 pour exit
*/
struct Sweep_x{
	int id;
	double x;
	double y;
	int is_void;
};

// this is an strucure which implements the 
// operator overlading 
struct Cmp_sweep_x { 
    bool operator()(
		struct Sweep_x const& swpx1, struct Sweep_x const& swpx2
	) { 
        return swpx1.x > swpx2.x; 
    } 
}; 

// this is an strucure which implements the 
// operator overlading 
struct Cmp_sweep_x_maxy { 
    bool operator()(
		struct Sweep_x const& swpx1, struct Sweep_x const& swpx2
	) { 
        return swpx1.y <= swpx2.y; 
    } 
}; 

struct Rectangle gen_rect_by_mid(
	double x,
	double y,
	double base,
	double height
) {
	return {
			   std::pair<double,double>(x - base / 2, y + height / 2),
			   std::pair<double,double>(x + base / 2, y + height / 2), 
			   std::pair<double,double>(x - base / 2, y - height / 2), 
		       std::pair<double,double>(x + base / 2, y - height / 2)
		   };
}

void print_coord(
	struct Rectangle r
) {
	std::cout << "upper_left : " 
			  << "("  
              << r.upper_left.first 
              << "," 
              << r.upper_left.second 
              << ")"  << std::endl;	
	std::cout << "upper_right : " 
			  << "("  
			  << r.upper_right.first 
			  << "," 
			  << r.upper_right.second 
			  << ")"  << std::endl;	
	std::cout << "lower_left : " 
			  << "("  
			  << r.lower_left.first 
			  << "," 
			  << r.lower_left.second 
			  << ")"  << std::endl;	
	std::cout << "lower_right : " 
			  << "("  
			  << r.lower_right.first 
			  << "," 
			  << r.lower_right.second 
			  << ")"  << std::endl;	
}


void print_swpx(struct Sweep_x * swpx_coords){
	if(swpx_coords == NULL) printf("NULL\n");
	else{
		std::cout << swpx_coords->id  << std::endl;
		std::cout << swpx_coords->x  << std::endl;
		std::cout << swpx_coords->y  << std::endl;
	}
	std::cout << "------------------"  << std::endl;
}
int main (){
	
	char c;
	double x, y, b, h, perimeter = 0, area = 0, max_y = 0;
	struct Sweep_x current_swpx, last_swpx;
	std::vector<int> track_swpx_aset;
	std::priority_queue<Sweep_x, std::vector<Sweep_x>, Cmp_sweep_x_maxy> track_maxy;
	std::priority_queue<Sweep_x, std::vector<Sweep_x>, Cmp_sweep_x_maxy> track_void_maxy;
	std::priority_queue<Sweep_x, std::vector<Sweep_x>, Cmp_sweep_x> swpx_coords;
	int id = 0, is_void = 0;

	while(std::cin >> c >> x >> y >> b >> h){
		if(b < 1 || h < 1) continue;

		/**
			Genere les 4 coordonne formant le rectangle
			presentement iterer
		*/
		struct Rectangle r = gen_rect_by_mid(x, y, b, h);
		
		is_void = c == 'p' ? 0 : 1;

		/**
			Ajout de la coordonner du coter upper_left
			avec l'event entry
		*/
		swpx_coords.push({
			id, 
			r.upper_left.first, 
			r.upper_left.second,
			is_void
		});

		/**
			Ajout de la coordonner du coter upper_right
			avec l'event exit
		*/
		swpx_coords.push({
			id, 
			r.upper_right.first, 
			r.upper_right.second,
			is_void
		});
	
		track_swpx_aset.push_back(-1);
		
		id++;
	}

	/**
		Sweep vertical
	*/
	while(!swpx_coords.empty()){
		
		//
		//	reference le top de swpx
		//	pour l'inserer dans l'active_set ou
		//	bien pour le supprimer de la liste
		//	de l'active set.
		//
		current_swpx = swpx_coords.top();

		//
		//	Regarde si le maixmum de la pile est toujours
		//	dans la liste de l'active set. Si c'est pas le cas
		//	on le supprime.
		//
				
		while(!track_maxy.empty() &&
			  track_swpx_aset.at(track_maxy.top().id) < 0){
			track_maxy.pop();
		}
			
		//
		//	Increment area
		//
		if(!track_maxy.empty()) {
			area += (current_swpx.x - last_swpx.x) * track_maxy.top().y;
			
			// regarde si le maximum de la void pile
			// est toujours dans liste de l'active set
			// si c'est pas le cas on le supprime
			while(!track_void_maxy.empty() &&
			 	  track_swpx_aset.at(track_void_maxy.top().id) < 0){
				track_void_maxy.pop();
			}

			// decrement l'aire du rectangle de la partie void
			if(!track_void_maxy.empty()){
				area -= (current_swpx.x - last_swpx.x) * track_void_maxy.top().y;
			}
		}
		
	//		si le vector a l'indice id de current est > 0
	//		ca signifie qu'il faut ajouter le current
	//		dans le max_y.
		
		if(track_swpx_aset.at(current_swpx.id) < 0){
			if(current_swpx.is_void) track_void_maxy.push(current_swpx);
			else track_maxy.push(current_swpx);
		}

		//
		//	si current == 1 alors ca signifie que l'evente
		//	du coter gauche a deja ete declenche donc on le met
		//	a -1. En revanche si current = -1 ca signie que
		//	l'event du coter gauche n'a pas ete encore declenche
		//	donc = 1.
		//

		track_swpx_aset.at(current_swpx.id) *= -1;
		
		last_swpx = current_swpx;
		
		swpx_coords.pop();
	}
	
	
/*	while(!swpx_coords.empty()){
		std::cout << swpx_coords.top().id  << std::endl;
		std::cout << swpx_coords.top().x  << std::endl;
		std::cout << swpx_coords.top().y  << std::endl;
		std::cout << "------------------"  << std::endl;
		swpx_coords.pop();
	}
*/	

	std::cout << "Aire = " << (area < 0 ? 0 : area) << std::endl; 
	std::cout << "Perimetre = " << (perimeter < 0 ? 0 : perimeter) << std::endl;

	return 0;
}
