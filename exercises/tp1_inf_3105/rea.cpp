#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Rectangle {
	int id;
	std::pair<double,double> upper_left;
	std::pair<double,double> upper_right;
	std::pair<double,double> lower_left;
	std::pair<double,double> lower_right;
};

struct Three_uplet{
	int first;    // id
	double second;// x or y (x_sorted = x  ---  y_sorted = y)
	double third; // x or y (x_sorted = y  ---  y_sorted = x)
	int is_void;
};

struct Rectangle gen_rect_by_mid(
	int id,
	double x,
	double y,
	double base,
	double height
) {
	return {   
			   id,
			   std::pair<double,double>(x - base / 2, y + height / 2),
			   std::pair<double,double>(x + base / 2, y + height / 2), 
			   std::pair<double,double>(x - base / 2, y - height / 2), 
		       std::pair<double,double>(x + base / 2, y - height / 2)
		   };
}

void print_coord(
	struct Rectangle r
) {
	std::cout << "upper_left : " 
			  << "("  
              << r.upper_left.first 
              << "," 
              << r.upper_left.second 
              << ")"  << std::endl;	
	std::cout << "upper_right : " 
			  << "("  
			  << r.upper_right.first 
			  << "," 
			  << r.upper_right.second 
			  << ")"  << std::endl;	
	std::cout << "lower_left : " 
			  << "("  
			  << r.lower_left.first 
			  << "," 
			  << r.lower_left.second 
			  << ")"  << std::endl;	
	std::cout << "lower_right : " 
			  << "("  
			  << r.lower_right.first 
			  << "," 
			  << r.lower_right.second 
			  << ")"  << std::endl;	
}


/**

	(x_sorted) on trie en ordre de x -> void -> y
	(y_sorted) on trie en ordre de y -> void -> x

	si x_sorted :
		si x1 < x2
			retourne x1
		sinon si x1 == x2
			si p1 est negatif retourne true
			sinon si p2 est negatif retour false
			sinon si y1 == y2
				retourne p1.id < p2.id
			retourne y1 < y2
		retourne false 
*/		
bool xy_sort (
	struct Three_uplet p1,
	struct Three_uplet p2
) {
	if(p1.second < p2.second)
		return true;
	else if(p1.second == p2.second){
		if(p1.is_void) return true;
		else if(p2.is_void) return false;
		else if(p1.third == p2.third)
			return p1.first < p2.first;
		return p1.third < p2.third;
	}
	return false;
}

int is_in_a_set_ver_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_x
) {
	return rect_ids_tracker_x.at(index) == 1;
}

int not_in_a_set_hor_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_y
) {
	return rect_ids_tracker_y.at(index) == -1;
}


int main (){
	
	char c;
	double x, y, b, h, perimeter = 0, area = 0, dx, dx_rot, dy, dy_rot;

	/**
		rect_id     : numero d'identification des rectangles
		cnt_swp_hor : Nombre de rectangle positif dans le range 
				      (utiliser pour le balayage horizontal)
		cnt_swp_ver : Nombre de rectangle positif dans le range 
				      (utiliser pour le balayage vertical)
		void_count  : Nombre de rectangle negatif dans le range
					  (utiliser pour le balayage horizontal)
	*/
	int rect_id = 0, cnt_swp_hor, cnt_swp_hor_rot, cnt_swp_ver, void_count, void_count_rot, index, index_rot, nb_gap_ver, nb_gap_ver_rot;

	/**
		x_sorted : tableau contenant les couple (id,x,y) 
			       trier en ordre croissant en faveur des
                   valeurs x
		y_sorted : Tableau contenant les couple (id,x,y)
				   trier en ordre croissant en faveur des 
				   valeurs de y
	*/
	std::vector<struct Three_uplet> x_sorted, y_sorted;

	/**
		rect_ids_tracker_x : Active_set du balayage vertical
		rect_ids_tracker_y : Active_set du balayage horizontal
		is_void_tracker    : Indique sur le rectangle a l'indice
							 i est un positif ou negatif rectangle.
	*/
	std::vector<int> rect_ids_tracker_x, rect_ids_tracker_y, is_void_tracker;
	
	/**
		x_sorted : tableau contenant les couple (id,x,y) 
			       trier en ordre croissant en faveur des
                   valeurs x
		y_sorted : Tableau contenant les couple (id,x,y)
				   trier en ordre croissant en faveur des 
				   valeurs de y
	*/
	std::vector<struct Three_uplet> x_sorted_rot, y_sorted_rot;

	/**
		rect_ids_tracker_x : Active_set du balayage vertical
		rect_ids_tracker_y : Active_set du balayage horizontal
		is_void_tracker    : Indique sur le rectangle a l'indice
							 i est un positif ou negatif rectangle.
	*/
	std::vector<int> rect_ids_tracker_x_rot, rect_ids_tracker_y_rot;

	while(std::cin >> c >> x >> y >> b >> h && c != 's') {
		if(b < 1 || h < 1) continue;

		// ajoute une nouvelle case memoire pour voir si la sweep line est dans ce rectangle
		rect_ids_tracker_x.push_back(-1);
		rect_ids_tracker_y.push_back(-1);
		is_void_tracker.push_back(c == 'p' ? 0 : 1);

		//genere les 4 point formant les couples (x,y) du rectangle
		struct Rectangle rect = gen_rect_by_mid(rect_id, x, y, b, h);

		// insert x1 du rectangle
		x_sorted.push_back({rect_id, rect.upper_left.first, rect.upper_left.second, is_void_tracker.at(is_void_tracker.size()-1)});

		//insert x2 du rectangle
		x_sorted.push_back({rect_id, rect.upper_right.first, rect.upper_right.second, is_void_tracker.at(is_void_tracker.size()-1)});

		//insert y1 du rectangle
		y_sorted.push_back({rect_id, rect.upper_left.second, rect.upper_left.first, is_void_tracker.at(is_void_tracker.size()-1)});

		//insert y2 du rectangle
		y_sorted.push_back({rect_id, rect.lower_right.second, rect.lower_right.first, is_void_tracker.at(is_void_tracker.size()-1)});

		//increment le rect id.
		rect_id++;
	}

	// triage de (x,y)_sorted en ordre croissant
	std::sort (x_sorted.begin(), x_sorted.end(), xy_sort);
	std::sort (y_sorted.begin(), y_sorted.end(), xy_sort);

	// utiliser pour le calcule du perimetre en rotation 90 degree.	
	rect_ids_tracker_x_rot = rect_ids_tracker_y; //  POUR PERIMETRE
	rect_ids_tracker_y_rot = rect_ids_tracker_x; //  POUR PERIMETRE


	std::cout << "ACTIVE_SET_X" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < rect_ids_tracker_x.size() ; i++){
		std::cout << "id " << i << " " << rect_ids_tracker_x.at(i) << std::endl;
	}
	std::cout << std::endl;
	
	std::cout << "ACTIVE_SET_Y" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < rect_ids_tracker_y.size() ; i++){
		std::cout << "id " << i << " : " << rect_ids_tracker_y.at(i) << std::endl;
	}	
	std::cout << std::endl;

	std::cout << "IS_VOID_TRACKER" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < is_void_tracker.size() ; i++){
		std::cout << "id " << i << " : " << is_void_tracker.at(i) << std::endl;
	}	
	std::cout << std::endl;

	std::cout << "X_SORTED" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < x_sorted.size() ; i++){
		std::cout << "id : " << x_sorted.at(i).first << " x  : " << x_sorted.at(i).second << std::endl;;
	}
	std::cout << std::endl;

	std::cout << "Y_SORTED" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < y_sorted.size() ; i++){
		std::cout << "id : " << y_sorted.at(i).first << " y  : " << y_sorted.at(i).second << std::endl;
	}	
	std::cout << std::endl;
		
	// balayage vertical
	for(st i = 0 ; i < x_sorted.size()-1 ; i++){
		struct Three_uplet x1 = x_sorted.at(i);
		struct Three_uplet x2 = x_sorted.at(i+1);
		struct Three_uplet x1_rot = y_sorted.at(i); //  POUR PERIMETRE
		struct Three_uplet x2_rot = y_sorted.at(i+1); //  POUR PERIMETRE
		struct Three_uplet y1, y1_rot; //  POUR PERIMETRE
		struct Three_uplet * last_event_y= NULL; // (utiliser pour le perimetre)
		struct Three_uplet * last_event_y_void = NULL;
		struct Three_uplet * last_event_y_rot = NULL;  //  POUR PERIMETRE
		struct Three_uplet * last;
		int has_perim_rot = 0;
		dx = x2.second - x1.second;
		dx_rot = x2_rot.second - x1_rot.second;// pour perimetre
		rect_ids_tracker_x.at(x1.first) *= -1;
		rect_ids_tracker_x_rot.at(x1_rot.first) *= -1; //  POUR PERIMETRE
		dy = cnt_swp_hor = void_count = 0;
		dy_rot = cnt_swp_hor_rot = void_count_rot = 0; //  POUR PERIMETRE
		nb_gap_ver = 0;
		nb_gap_ver_rot = 0;  //  POUR PERIMETRE
		//balayage horizontal
		for(st j = 0 ; j < y_sorted.size() ; j++){
			index = y_sorted.at(j).first;
			index_rot = x_sorted.at(j).first; //  POUR PERIMETRE

///////////////////////////////////////////
			if(dx_rot > 0 && is_in_a_set_ver_sweep(index_rot, rect_ids_tracker_x_rot)){
				if(not_in_a_set_hor_sweep(index_rot, rect_ids_tracker_y_rot)){
//					std::cout << "entry : " << x_sorted.at(j).first <<std::endl;
					if(is_void_tracker.at(index_rot) == 1) {
						if(cnt_swp_hor_rot > 0 && void_count_rot == 0){
							has_perim_rot = 1;
						}
						void_count_rot++;
					} else{
						if(cnt_swp_hor_rot == 0) {
							if(last_event_y_rot != NULL && x_sorted.at(j).second != last_event_y_rot->second){
								nb_gap_ver_rot++;
							}
							last_event_y_rot = NULL;
							if(void_count_rot == 0){
								y1_rot = x_sorted.at(j);
							}
						}
						cnt_swp_hor_rot++;
					}
				} else{
//					std::cout << "exit : " << x_sorted.at(j).first  << std::endl;
					if(is_void_tracker.at(index_rot) == 1){
						void_count_rot--;
						if(void_count_rot == 0 && cnt_swp_hor_rot > 0){
							y1_rot = x_sorted.at(j);
						}
					} else{
						cnt_swp_hor_rot--;
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0){
							has_perim_rot = 1;
						}
						last_event_y_rot = &x_sorted.at(j);
					}
				}
				rect_ids_tracker_y_rot.at(x_sorted.at(j).first) *= -1;
			}

/////////////////////////////////
			if(dx > 0 && is_in_a_set_ver_sweep(index, rect_ids_tracker_x)){
				if(not_in_a_set_hor_sweep(index, rect_ids_tracker_y)){
					std::cout << "entry : " << y_sorted.at(j).first <<std::endl;
					if(is_void_tracker.at(index) == 1) {
						if(cnt_swp_hor > 0 && void_count == 0){
							dy += y_sorted.at(j).second - y1.second;
						}
						void_count++;
					} else{
						if(cnt_swp_hor == 0) {
							if(last_event_y != NULL && y_sorted.at(j).second != last_event_y->second){
								nb_gap_ver++;
							}
							last_event_y = NULL;
							if(void_count == 0){
								y1 = y_sorted.at(j);
							}
						}
						cnt_swp_hor++;
					}
				} else{
					std::cout << "exit : " << y_sorted.at(j).first  << std::endl;
					if(is_void_tracker.at(index) == 1){
						void_count--;
						if(void_count == 0 && cnt_swp_hor > 0){
							y1 = y_sorted.at(j);
						}
					} else{
						cnt_swp_hor--;
						if(cnt_swp_hor == 0 && void_count == 0){
							dy +=  y_sorted.at(j).second - y1.second;
						}
						last_event_y = &y_sorted.at(j);
					}
				}
				rect_ids_tracker_y.at(y_sorted.at(j).first) *= -1;
			}
		}
		if(dx > 0) area += dx * dy;
	//	printf("(%d) Nombre de gab entre %f et %f = %d\n",is_void_tracker.at(x1.first), x1.second, x2.second, nb_gap_ver);
		if(dx > 0 && dy > 0) perimeter += 2 * (nb_gap_ver * dx + dx) ;
		if(dx_rot > 0 && has_perim_rot) perimeter += 2 * (nb_gap_ver_rot * dx_rot + dx_rot);
	}

	std::cout << "Aire = " << (area < 0 ? 0 : area) << std::endl; 
	std::cout << "Perimetre = " << (perimeter < 0 ? 0 : perimeter) << std::endl;
	return 0;
}
