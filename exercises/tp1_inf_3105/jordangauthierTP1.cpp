#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

/**
	Structure representant un evenement de type entre ou sortie.
	Un evenement entre signifique le coter gauche d'un rectangle
	ou le bas du rectangle. Un evenement sortant represente le cote
	droit d'un rectangle ou le haut d'un rectangle.

	@param id numero d'identification unique associer a un rectangle
	@param first Une valeur x ou y qui depend de l'orientation du balayage.
	@param second Une valeur x ou y qui depend de l'orientation du balayage
	@param is_void Une valeur de 1 signifie un rectangle negatif une valeur
		           de 0 signifie un rectangle positif
	@param is_entry Une valeur de 1 signifie un evenement entrant.
                    Une valeur de 0 signifie un evenement sortant.
*/
struct Event{
	int first; 
	double second;
	double third;
	int is_void;
	int is_entry;
};

/**
	Retourne l'evenement trier entre deux de ceux-ci
	selon certaine specification.

	@param p1 structure de type evenement representant l'evenement 1.
	@param p2 structure de type evenement representant l'evenement 2
*/
bool xy_sort (
	struct Event p1,
	struct Event p2
) {
	if(p1.second < p2.second) return true;
	else if(p1.second == p2.second){
		if(p1.is_void){
			if(p1.is_entry) return true;
			else return false;
		}else if(p2.is_void){
			if(p2.is_entry) return false;
			else return true;
		}
		if(p1.is_entry) return true;
		else if(p2.is_entry) return false;
		if(p1.third == p2.third) return p1.first < p2.first;
		return p1.third < p2.third;
	}
	return false;
}

/**
	Echange la valeur pointer par les deux pointeur
	de type event.

	@param p1 structure de type evenement
	@param p2 structure de type evenement
*/
void swap(
	struct Event * p1, 
	struct Event * p2
) { 
    struct Event t = *p1; 
    *p1 = *p2; 
    *p2 = t; 
} 

/**
	
*/  
int partition (
	std::vector<struct Event> &arr, 
	int low, 
	int high
) { 
    struct Event pivot = arr.at(high);
    int i = (low - 1);
    for (int j = low; j <= high- 1; j++) { 
        if (xy_sort(arr.at(j), pivot)) { 
            i++; 
            swap(&arr.at(i), &arr.at(j)); 
        } 
    } 
    swap(&arr.at(i + 1), &arr.at(high)); 
    return (i + 1); 
} 
  
/**
	Trie les evenements de arr conformement a la fonction xy_sort.

	@param arr refereence sur un vector d'evenement
	@param low index de depart du triage
	@param high index de fin du triage
*/
void quickSort(
	std::vector<struct Event> &arr, 
	int low, 
	int high
) { 
    if (low < high) { 
        int pi = partition(arr, low, high); 
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
} 

/**
	Retourne 1 si le rectangle a l'index index est setter a 1
	dans l'active_set passer en parametre.

	@param index index (id du rectangle) pour lequelle la valeur sera verifier
	@param rect_ids_tracker_x active set 
*/
int is_in_a_set_ver_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_x
) {
	return rect_ids_tracker_x.at(index) == 1;
}

/**
	Retourne 1 si le rectangle n'est pas dans l'active set du balayage horizontal.

	@param index id du rectangle
	@param active set y
*/
int not_in_a_set_hor_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_y
) {
	return rect_ids_tracker_y.at(index) == -1;
}

/**
	Execute les action du line sweep lorsque le rectangle iterer
	est deja dans l'active set vertical et que l'evenement est positif.

	Les actions poser son decrementation de cnt, incrementation possible
	de dy et du perimetre.

	@param cnt variable qui compte le nombre de retangle dans l'active set vertical
			   cette variable est decrementer de 1.
	@param void_cnt l'incrementation du perimetre et de dy sera fait si ce parametre
					 a une valeur de 0
	@param dy represente le delta y (sera possiblement incrementer)
	@param dx represente le delta x du range du balayage vertical
	@param perimeter represente le perimetre (cette valeur sera possiblement incrementer)
	@param y1 valeur soustrait de y2 pour l'incrementation de dy
	@param y2 valeur sur laquelle on soustrait y1 pour l'incrementation de dy.
*/
void in_set_positive_rectangle(
	int &cnt,
	int void_cnt,
	double &dy,
	double dx,
	double &perimeter,
	double y2,
	double y1
) {
	cnt--;
	if(cnt == 0 && void_cnt == 0){
		dy +=  y2 - y1;
		perimeter += dx;
	}
}

/**
	Execute les action du line sweep lorsque le rectangle iterer
	est deja dans l'active set vertical et que l'evenement est negatif.

	Les actions poser son decrementation de void_count, affectation de y1 au premier y,
	incrementation du perimetre

	@param cnt variable qui compte le nombre de retangle dans l'active set vertical
			   cette variable est decrementer de 1.
	@param void_cnt le nombre de rectangle negatif dans le range
	@param dx represente le delta x du range du balayage vertical
	@param perimeter represente le perimetre (cette valeur sera possiblement incrementer)
	@param y1 pointeur pointant sur le y1
	@param y1_assign evenement a assigner a y1
*/
void in_set_negative_rectangle(
	int &void_cnt,
	int cnt,
	struct Event *y1,
	struct Event &y1_assign,
	double &perimeter,
	double dx
) {
	void_cnt--;
	if(void_cnt == 0 && cnt > 0){
		*y1 = y1_assign;
		perimeter += dx;
	}
}

/**
	Execute les action du line sweep quand l'evenement est deja dans l'active set
	vertical.

	@param event_at_j evenement presentement iterer sur lequelle les action seront effectuer
	@param cnt variable qui compte le nombre de retangle dans l'active set vertical
			   cette variable est decrementer de 1.
	@param void_cnt l'incrementation du perimetre et de dy sera fait si ce parametre
					 a une valeur de 0
	@param dy represente le delta y (sera possiblement incrementer)
	@param dx represente le delta x du range du balayage vertical
	@param perimeter represente le perimetre (cette valeur sera possiblement incrementer)
	@param y1 valeur soustrait de y2 pour l'incrementation de dy
*/
void in_active_set(
	struct Event & event_at_j,
	struct Event * y1,
	int & cnt,
	int & void_cnt,
	double & perimeter,
	double &dy,
	double dx	
) {
	if(event_at_j.is_void){
		in_set_negative_rectangle(void_cnt, cnt, &*y1,
								  event_at_j, perimeter, dx);
	} else{
		in_set_positive_rectangle(cnt, void_cnt, dy,
								  dx, perimeter, event_at_j.second,
								  y1->second);
	}
}

/**
	Execute les actions des evenements qui ne sont pas l'active set et qui sont positif.
	Si le nombre de rectangle positif est de 0 ainsi que le nombre de rectangle negatif,
	le perimetre est incrementer de dx et y1 = y1_assign. cnt est incrementer de 1.

	@param Un entier cnt le nombre de rectangle positif dans le range en ce moment.
	@param Un entier void_cnt nombre de rectangles negatif dans le range en ce moment (sujet a changement)
	@param perimeter un double representent le perimetre (sujet a changement)
	@param un double repreentant delta x du range
	@param y1 Un evenement representant le premier y
	@param y1_assign l'evenement qui sera affecter a y1.
*/
void not_in_set_positive_rectangle(
	int & cnt,
	int void_cnt,
	double & perimeter,
	double dx,
	struct Event * y1,
	struct Event & y1_assign
) {
	if(cnt == 0 && void_cnt == 0) {
		perimeter += dx;
		*y1 = y1_assign;
	}
	cnt++;
}


/**
	Si le nombre de rectangle positif est plus grand que 0 et que le nombre
	de rectangle negatif est egal a 0 alors le perimetre est increment de dx et
	dy est incrementer de y2 - y1. void_cnt est incrementer de 1.

	@param cnt Un entier representant le nombre de rectangle positif
	@param void_cnt le nombre de rectangle negatif
	@param perimeter Un double representant le perimetre
	@param dy Un double representant delta y
	@param dx Un double representant delta x
	@param y2 Un double representant la valeur second de l'event y2
	@param y2 un double representant la valeur second de l'event y1 
*/
void not_in_set_negative_rectangle(
	int cnt,
	int & void_cnt,
	double & perimeter,
	double & dy,
	double dx,
	double y2,
	double y1
) {
	if(cnt > 0 && void_cnt == 0){
		perimeter += dx;
		dy += y2 - y1;
	}
	void_cnt++;
}

/**
	Execute les actions des evenements positif et negatif 
	qui ne sont pas dans l'active set du balayage horizontal

	@param Un Event event_at_j l'evenement presentement iterer
	@param Un Event y1 l'evenement qui est le premier y du delta y
	@param Un entier cnt de rectanglepositif presentement dans le range
	@param Un entier void_cnt le nombre de rectangle negatif presentement dans le range
	@param Un double representant delta y
	@param Un double representant delta x
*/
void not_in_active_set(
	struct Event & event_at_j,
	struct Event * y1,
	int & cnt,
	int & void_cnt,
	double & dy,
	double & perimeter,
	double dx
) {
	if(event_at_j.is_void) {
		not_in_set_negative_rectangle(cnt, void_cnt, perimeter, dy, dx,
									  event_at_j.second, y1->second);
	} else{
		not_in_set_positive_rectangle(cnt, void_cnt, perimeter, 
									  dx, &*y1, event_at_j);
	}
}

/**
	Fonction global appelant les fonctions sous-jacentes d'actions
	d'evenement positif et negatif du balayage horizontal.

	@param index Un entier representant l'id du rectangle presentement iterer
	@param cnt Un entier representant le nombre de rectangle positif dans le range
	@param void_cnt Un entier representant le nombre de rectangle negatif.
	@param perimeter Un double representant le perimetre
	@param dy un Double representant delta y
	@param Un doube representant delta x
	@param un vector d'entier representant l'active_set_y (balayage horizontal)
	@param L'Event presentement iterer
*/
void horizontal_swp(
	int index,
	int & cnt,
	int & void_cnt,
	double & perimeter,
	double & dy,
	double dx,
	std::vector<int> & active_set_y,
	struct Event & event_at_j,
	struct Event * y1
) {
	if(not_in_a_set_hor_sweep(index, active_set_y)){
		not_in_active_set(event_at_j, &*y1, cnt, 
						  void_cnt, dy, perimeter,
						  dx);				
	} else{
		in_active_set(event_at_j, &*y1, cnt, void_cnt, 
					  perimeter, dy, dx);
	}
	active_set_y.at(event_at_j.first) *= -1;
}

/**
	Execute l'action lorsque l'evenement iterer est dans l'active set horizontal
	est qu'il est positif et que le balayage est effectuer en rotation 90 degree.
	cnt est decrementer de 1, perimeter peu possiblement etre incrementer.	

	@param cnt Un entier representant le nombre de rectangle positif
	@param void_cnt Un entier representant le nombre de rectangle negatif
	@param dx Un double representant delta x
	@param perimeter Un double representant le perimetre.
*/
void in_set_positive_rectangle_rot(
	int &cnt,
	int void_cnt,
	double dx,
	double &perimeter
) {
	cnt--;
	if(cnt == 0 && void_cnt == 0){
		perimeter += dx;
	}
}

/**
	Execute les action du balayage horizontal en rotation 90 degree
	des evenements des rectangles negatif qui ne sont pas dans l'active set.
	void_cnt est decrementer de 1, perimeter est possiblement incrementer.

	@param void_cnt Un entier representant le nombre de rectangle negatif
	@param cnt Un entier representant le nombre de rectangle positif
	@param perimeter Un double representant le perimetre
	@param dx Un double representant delta x
*/
void in_set_negative_rectangle_rot(
	int &void_cnt,
	int cnt,
	double &perimeter,
	double dx
) {
	void_cnt--;
	if(void_cnt == 0 && cnt > 0){
		perimeter += dx;
	}
}

/**
	Execute les actions des evenements du balayage horizontal en rotation 90 degree
	qui sont presentement dans l'active set.

	Contient les appelles des fonctions in_set_negative_rectangle_rot et
	in_set_positive_rectangle_rot

	@param event_at_j L'Event presentement iterer
	@param cnt Un entier representant le nombre de rectangle positif.
	@param void_cnt Un entier representant le nombre de rectangle negatif.
	@param perimeter Un double representant le perimetre.
	@param dx Un double representant delta x
*/
void in_active_set_rot(
	struct Event & event_at_j,
	int & cnt,
	int & void_cnt,
	double & perimeter,
	double dx	
) {
	if(event_at_j.is_void){
		in_set_negative_rectangle_rot(void_cnt, cnt, 
									  perimeter, dx);
	} else{
		in_set_positive_rectangle_rot(cnt, void_cnt, 
									  dx, perimeter);
	}
}

/**
	Incremente le perimetre si le nombre de rectangle positif est egal a 0
	et si le nombre de rectangle negatif est egal a 0.

	@param cnt Un entier representant le nombre de rectangle positif
	@param void_cnt Un entier representant le nombre de rectagnle negatif
	@param perimeter Un double representant le perimetre
	@param dx Un double representant delta x
*/
void not_in_set_positive_rectangle_rot(
	int & cnt,
	int void_cnt,
	double & perimeter,
	double dx
) {
	if(cnt == 0 && void_cnt == 0)
		perimeter += dx;
	cnt++;
}

/**
	Increment le perimetre de dx si le nombre de rectangle positif est plsu grand que 0
	et si le nombre de rectangle negatif est egal a 0.

	@param cnt Un entier representant le nombre de rectangle positif.
	@param void_cnt Un entier representant le nombre de rectangle negatif.
	@param perimeter Un double representant le perimetre.
	@param dx Un double representant delta x
*/
void not_in_set_negative_rectangle_rot(
	int cnt,
	int & void_cnt,
	double & perimeter,
	double dx
) {
	if(cnt > 0 && void_cnt == 0)
		perimeter += dx;
	void_cnt++;
}

/**
	Execute les actions des evenements qui sont postif et negatif et qui ne sont pas dans
	l'active set.
	
	contient les appelles des fonction not_in_set_negative_rectangle_rot et 
	not_in_set_positive_rectangle_rot.

	@param event_at_j L'Event presentement iterer
	@param cnt Un entier representant le nombre de rectangle positif.
	@param void_cnt Un entier representant le nombre de rectangle negatif.
	@param perimeter Un double representant le perimetre.
	@param dx Un double representant delta x.
*/
void not_in_active_set_rot(
	struct Event & event_at_j,
	int & cnt,
	int & void_cnt,
	double & perimeter,
	double dx
) {
	if(event_at_j.is_void) {
		not_in_set_negative_rectangle_rot(cnt, void_cnt, perimeter, dx);
	} else{
		not_in_set_positive_rectangle_rot(cnt, void_cnt, perimeter, dx);
	}
}

/**
	Fonction global appelant les fonctions sous-jacentes d'actions
	d'evenement positif et negatif du balayage horizontal effectuer
	en rotation 90 degree.

	@param index id du rectangle de l'evenement associer.
	@param active_set_y vector d'entier representant l'active set
	@param event_at_j l'Event presentement iterer.
	@param void_cnt Un entier representant le nombre de rectangle negatif
	@param cnt Un entier representant le nombre de rectangle positif.
	@param dx Un double representant delta x
	@param perimeter Un double representant le perimetre.
*/
void rotated_horizontal_swp(
	int index,
	std::vector<int> & active_set_y,
	struct Event & event_at_j,
	int & void_cnt,
	int & cnt,
	double dx,
	double & perimeter
) {
	if(not_in_a_set_hor_sweep(index, active_set_y)){
		not_in_active_set_rot(event_at_j, cnt, void_cnt, perimeter, dx);	
	} else{
		in_active_set_rot(event_at_j, cnt, void_cnt, perimeter, dx);
	}
	active_set_y.at(event_at_j.first) *= -1;

}

void ask_file(char file_name[], FILE ** file){
	int file_name_ok = 0;

	do{
		std::cout << "Entrez le nom du fichier : ";
		scanf("%s",file_name);
		*file = fopen(file_name, "r");
		if(*file == NULL) {
			fprintf(stderr, "Le nom du fichier n'existe pas ou il ne contient pas l'extension .txt\n");
			file_name_ok = 0;
		}else 
			file_name_ok = 1;
	}while(!file_name_ok);
	fclose(*file);
}

int main (){
	char c, p_or_n;
	double x, y, base, height, perimeter = 0, area = 0, dx, dx_rot, dy, dy_rot, l_side, r_side, u_side, b_side;
	int rect_id = 0, cnt_swp_hor, cnt_swp_hor_rot, cnt_swp_ver, void_count, void_count_rot, index, index_rot, all_ok = 1;
	std::vector<struct Event> x_events, y_events;
	std::vector<int> rect_ids_tracker_x, rect_ids_tracker_y;
	std::vector<struct Event> x_events_rot, y_events_rot;
	std::vector<int> rect_ids_tracker_x_rot, rect_ids_tracker_y_rot;
	char file_name[100];
	FILE * file;
	ask_file(file_name, &file);
	std::ifstream in(file_name);
    std::streambuf *cinbuf = std::cin.rdbuf();
    std::cin.rdbuf(in.rdbuf());
	while(std::cin >> c >> x >> y >> base >> height && all_ok) {
		if(base < 0 || height < 0){
			fprintf(stderr, "Erreur : La base et la hauteur doit etre > 0.");
			all_ok = 0;
		}
		p_or_n = c == 'p' ? 0 : 1;
		l_side = x - base / 2;
		r_side = x + base / 2;
		u_side = y + height / 2;
		b_side = y - height / 2;
		rect_ids_tracker_x.push_back(-1);
		rect_ids_tracker_y.push_back(-1);
		x_events.push_back({rect_id, l_side, u_side, p_or_n, 1});
		x_events.push_back({rect_id, r_side, u_side, p_or_n, 0});
		y_events.push_back({rect_id, u_side, l_side, p_or_n, 0});
		y_events.push_back({rect_id, b_side, r_side, p_or_n, 1});
		rect_id++;
	}
	if(all_ok){
		if(x_events.size() > 0){
			quickSort(x_events, 0, x_events.size()-1);
			quickSort(y_events, 0, y_events.size()-1);
		}
		rect_ids_tracker_x_rot = rect_ids_tracker_y;
		rect_ids_tracker_y_rot = rect_ids_tracker_x;
		for(st i = 0 ; i < x_events.size()-1 ; i++){
			struct Event y1, y1_rot;
			dx = x_events.at(i+1).second - x_events.at(i).second;
			dx_rot = y_events.at(i+1).second - y_events.at(i).second;
			rect_ids_tracker_x.at(x_events.at(i).first) *= -1;
			rect_ids_tracker_x_rot.at(y_events.at(i).first) *= -1;
			dy = cnt_swp_hor = void_count = 0;
			dy_rot = cnt_swp_hor_rot = void_count_rot = 0;
			for(st j = 0 ; j < y_events.size() ; j++){
				index = y_events.at(j).first;
				index_rot = x_events.at(j).first;
				if(dx_rot > 0 && is_in_a_set_ver_sweep(index_rot, rect_ids_tracker_x_rot)){
					rotated_horizontal_swp(index_rot, rect_ids_tracker_y_rot, 
								       x_events.at(j), void_count_rot, 
                                       cnt_swp_hor_rot, dx_rot, perimeter);
				}
				if(dx > 0 && is_in_a_set_ver_sweep(index, rect_ids_tracker_x)){
					horizontal_swp(index, cnt_swp_hor, void_count, perimeter, dy, dx, 
						       	rect_ids_tracker_y, y_events.at(j), &y1);
				}
			}
			if(dx > 0) area += dx * dy;
		}
		printf("aire : %f\n", area);
		printf("perimetre : %f\n", perimeter);
	}
	return 0;
}
