#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Point{
	double x;
	double y;
};

struct Rectangle {
	Point p1;
	Point p2;
	Point p3;
	Point p4;
	double mid_x;
	double mid_y;
	double base;
	double height;
	double left_x;
	double right_x;
	double upper_y;
	double lower_y;
	int is_odd;
	int is_void;
};

struct Rectangle calculate_points(
	double x,
	double y,
	double b,
	double h
) {
	struct Point p1, p2, p3, p4;

	p1.x  = p3.x  = x - b / 2;
	p2.x  = p4.x  = x + b / 2;
	p1.y = p2.y = y + h / 2;
	p3.y = p4.y = y - h / 2;

	return {.p1 = p1,
		    .p2 = p2, 
            .p3 = p3, 
            .p4 = p4,
			.mid_x = x,
			.mid_y = y,
			.base = b,
			.height = h,
			.left_x = x - b / 2,
			.right_x = x + b / 2,
			.upper_y = y + h / 2,
			.lower_y = y - h / 2,
			.is_odd = 1};
}

int overlap(
	struct Rectangle r1,
	struct Rectangle r2
) {
	return   !(r1.left_x > r2.right_x ||
	   		 r1.upper_y < r2.lower_y ||
	         r1.right_x < r2.left_x ||
	         r1.lower_y > r2.upper_y);
}

double intersect_area(
	struct Rectangle r1,
	struct Rectangle r2
) {
	return std::abs(std::min(r1.upper_y, r2.upper_y) -
		   std::max(r1.lower_y, r2.lower_y)) *
		   std::abs(std::max(r1.left_x, r2.left_x) -
		   std::min(r1.right_x, r2.right_x));
} 

struct Rectangle intersect_rectangle(
	struct Rectangle current,
	struct Rectangle comparator
) {
	struct Point p1, p2, p3, p4;

	p1.x = p3.x = std::max(current.left_x, comparator.left_x);
	p2.x = p4.x = std::min(current.right_x, comparator.right_x);
	p1.y = p2.y = std::min(current.upper_y, comparator.upper_y);
	p3.y = p4.y = std::max(current.lower_y, comparator.lower_y);
	
	return {.p1 = p1,
		    .p2 = p2, 
            .p3 = p3, 
            .p4 = p4,
			.mid_x = -1,
			.mid_y = -1,
			.base = std::abs(p2.x - p1.x),
			.height = std::abs(p2.y - p4.y),
			.left_x = p1.x,
			.right_x = p2.x,
			.upper_y = p1.y,
			.lower_y = p4.y,
			.is_odd = comparator.is_odd < 0 ? 1 : -1,
			.is_void = current.is_void};
}

int main (){
	char c;
	double x, y, b, h, perimeter = 0, area = 0;
	std::vector<struct Rectangle> rectangles, x_rect, segments;
	struct Rectangle current, x_r;
	while(std::cin >> c >> x >> y >> b >> h && c != 's'){
		if(b < 1 || h < 1) continue;		
		current = calculate_points(x, y, b, h); // creer un rectangle avec les inf
		if(c == 'p') {
			area += b * h; // ajoute l'aire du rectangle iterer
			perimeter += 2 * (b + h);
		}
		x_rect.push_back(current); // push ce rectangle dans notre array de rectangle ainsi que de intersect rect
		for(int i = x_rect.size() - 2 ; i > -1 ; i--){ // boucle de lalgo principal
			if(overlap(current,x_rect.at(i))){ //
				x_r = intersect_rectangle(current,x_rect.at(i));
				area += x_r.is_odd * x_r.base * x_r.height;
				perimeter += x_r.is_odd * 2 * (x_r.base + x_r.height);
				x_rect.push_back(x_r);
			}
		}
	}
	std::cout << "Aire = " << area << std::endl; 
	std::cout << "Perimetre = " << perimeter  << std::endl;
	return 0;
}

