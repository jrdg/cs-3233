#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Rectangle {
	int id;
	std::pair<double,double> upper_left;
	std::pair<double,double> upper_right;
	std::pair<double,double> lower_left;
	std::pair<double,double> lower_right;
};

struct Three_uplet{
	int first;    // id
	double second;// x or y (x_sorted = x  ---  y_sorted = y)
	double third; // x or y (x_sorted = y  ---  y_sorted = x)
	int is_void;
	int is_entry;
};

struct Rectangle gen_rect_by_mid(
	int id,
	double x,
	double y,
	double base,
	double height
) {
	return {   
			   id,
			   std::pair<double,double>(x - base / 2, y + height / 2),
			   std::pair<double,double>(x + base / 2, y + height / 2), 
			   std::pair<double,double>(x - base / 2, y - height / 2), 
		       std::pair<double,double>(x + base / 2, y - height / 2)
		   };
}


bool xy_sort (
	struct Three_uplet p1,
	struct Three_uplet p2
) {
	if(p1.second < p2.second) return true;
	else if(p1.second == p2.second){
		if(p1.is_void){
			if(p1.is_entry) return true;
			else return false;
		}else if(p2.is_void){
			if(p2.is_entry) return false;
			else return true;
		}
		if(p1.is_entry) return true;
		else if(p2.is_entry) return false;
		if(p1.third == p2.third) return p1.first < p2.first;
		return p1.third < p2.third;
	}
	return false;
}

void swap(
	struct Three_uplet * a, 
	struct Three_uplet * b
) { 
    struct Three_uplet t = *a; 
    *a = *b; 
    *b = t; 
} 
  
int partition (
	std::vector<struct Three_uplet> &arr, 
	int low, 
	int high
) { 
    struct Three_uplet pivot = arr.at(high);
    int i = (low - 1);
    for (int j = low; j <= high- 1; j++) { 
        if (xy_sort(arr.at(j), pivot)) { 
            i++; 
            swap(&arr.at(i), &arr.at(j)); 
        } 
    } 
    swap(&arr.at(i + 1), &arr.at(high)); 
    return (i + 1); 
} 
  
void quickSort(
	std::vector<struct Three_uplet> &arr, 
	int low, 
	int high
) { 
    if (low < high) { 
        int pi = partition(arr, low, high); 
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
} 

int is_in_a_set_ver_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_x
) {
	return rect_ids_tracker_x.at(index) == 1;
}

int not_in_a_set_hor_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_y
) {
	return rect_ids_tracker_y.at(index) == -1;
}


int main (){
	
	char c;
	double x, y, b, h, perimeter = 0, area = 0, dx, dx_rot, dy, dy_rot;

	/**
		rect_id     : numero d'identification des rectangles
		cnt_swp_hor : Nombre de rectangle positif dans le range 
				      (utiliser pour le balayage horizontal)
		cnt_swp_ver : Nombre de rectangle positif dans le range 
				      (utiliser pour le balayage vertical)
		void_count  : Nombre de rectangle negatif dans le range
					  (utiliser pour le balayage horizontal)
	*/
	int rect_id = 0, cnt_swp_hor, cnt_swp_hor_rot, cnt_swp_ver, void_count, void_count_rot, index, index_rot, nb_gap_ver, nb_gap_ver_rot;

	/**
		x_sorted : tableau contenant les couple (id,x,y) 
			       trier en ordre croissant en faveur des
                   valeurs x
		y_sorted : Tableau contenant les couple (id,x,y)
				   trier en ordre croissant en faveur des 
				   valeurs de y
	*/
	std::vector<struct Three_uplet> x_sorted, y_sorted;

	/**
		rect_ids_tracker_x : Active_set du balayage vertical
		rect_ids_tracker_y : Active_set du balayage horizontal
		is_void_tracker    : Indique sur le rectangle a l'indice
							 i est un positif ou negatif rectangle.
	*/
	std::vector<int> rect_ids_tracker_x, rect_ids_tracker_y, is_void_tracker;
	
	/**
		x_sorted : tableau contenant les couple (id,x,y) 
			       trier en ordre croissant en faveur des
                   valeurs x
		y_sorted : Tableau contenant les couple (id,x,y)
				   trier en ordre croissant en faveur des 
				   valeurs de y
	*/
	std::vector<struct Three_uplet> x_sorted_rot, y_sorted_rot;

	/**
		rect_ids_tracker_x : Active_set du balayage vertical
		rect_ids_tracker_y : Active_set du balayage horizontal
		is_void_tracker    : Indique sur le rectangle a l'indice
							 i est un positif ou negatif rectangle.
	*/
	std::vector<int> rect_ids_tracker_x_rot, rect_ids_tracker_y_rot;

	while(std::cin >> c >> x >> y >> b >> h ) {
		// ajoute une nouvelle case memoire pour voir si la sweep line est dans ce rectangle
		rect_ids_tracker_x.push_back(-1);
		rect_ids_tracker_y.push_back(-1);
		is_void_tracker.push_back(c == 'p' ? 0 : 1);

		//genere les 4 point formant les couples (x,y) du rectangle
		struct Rectangle rect = gen_rect_by_mid(rect_id, x, y, b, h);

		// insert x1 du rectangle
		x_sorted.push_back({rect_id, rect.upper_left.first, rect.upper_left.second, is_void_tracker.at(is_void_tracker.size()-1), 1});

		//insert x2 du rectangle
		x_sorted.push_back({rect_id, rect.upper_right.first, rect.upper_right.second, is_void_tracker.at(is_void_tracker.size()-1), 0});

		//insert y1 du rectangle
		y_sorted.push_back({rect_id, rect.upper_left.second, rect.upper_left.first, is_void_tracker.at(is_void_tracker.size()-1), 0});

		//insert y2 du rectangle
		y_sorted.push_back({rect_id, rect.lower_right.second, rect.lower_right.first, is_void_tracker.at(is_void_tracker.size()-1) , 1});

		//increment le rect id.
		rect_id++;
	}

	// triage de (x,y)_sorted en ordre croissant
	//std::sort (x_sorted.begin(), x_sorted.end(), xy_sort);
	//std::sort (y_sorted.begin(), y_sorted.end(), xy_sort);

	quickSort(x_sorted, 0, x_sorted.size()-1);
	quickSort(y_sorted, 0, y_sorted.size()-1);

	// utiliser pour le calcule du perimetre en rotation 90 degree.	
	rect_ids_tracker_x_rot = rect_ids_tracker_y; //  POUR PERIMETRE
	rect_ids_tracker_y_rot = rect_ids_tracker_x; //  POUR PERIMETRE


/*	std::cout << "ACTIVE_SET_X" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < rect_ids_tracker_x.size() ; i++){
		std::cout << "id " << i << " " << rect_ids_tracker_x.at(i) << std::endl;
	}
	std::cout << std::endl;
	
	std::cout << "ACTIVE_SET_Y" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < rect_ids_tracker_y.size() ; i++){
		std::cout << "id " << i << " : " << rect_ids_tracker_y.at(i) << std::endl;
	}	
	std::cout << std::endl;

	std::cout << "IS_VOID_TRACKER" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < is_void_tracker.size() ; i++){
		std::cout << "id " << i << " : " << is_void_tracker.at(i) << std::endl;
	}	
	std::cout << std::endl;

	std::cout << "X_SORTED" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < x_sorted.size() ; i++){
		std::cout << "id : " << x_sorted.at(i).first << " x  : " << x_sorted.at(i).second << " is_entry : " << x_sorted.at(i).is_entry << " is_void : " << x_sorted.at(i).is_void  << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Y_SORTED" << std::endl;
	std::cout << "----------------------" << std::endl;
	for(st i = 0 ; i < y_sorted.size() ; i++){
		std::cout << "id : " << y_sorted.at(i).first << " y  : " << y_sorted.at(i).second << " is_entry : " << y_sorted.at(i).is_entry << " is_void : " << y_sorted.at(i).is_void << std::endl;
	}	
	std::cout << std::endl;*/
		
	// balayage vertical
	for(st i = 0 ; i < x_sorted.size()-1 ; i++){
		struct Three_uplet x1 = x_sorted.at(i);
		struct Three_uplet x2 = x_sorted.at(i+1);
		struct Three_uplet x1_rot = y_sorted.at(i); //  POUR PERIMETRE
		struct Three_uplet x2_rot = y_sorted.at(i+1); //  POUR PERIMETRE
		struct Three_uplet y1, y1_rot; //  POUR PERIMETRE
		dx = x2.second - x1.second;
		dx_rot = x2_rot.second - x1_rot.second;// pour perimetre
		rect_ids_tracker_x.at(x1.first) *= -1;
		rect_ids_tracker_x_rot.at(x1_rot.first) *= -1; //  POUR PERIMETRE
		dy = cnt_swp_hor = void_count = 0;
		dy_rot = cnt_swp_hor_rot = void_count_rot = 0; //  POUR PERIMETRE
//		std::cout << "x1 : "  << x1.second << " x2 : " << x2.second << std::endl;
//		std::cout << "..............................." << std::endl;
		//balayage horizontal
		for(st j = 0 ; j < y_sorted.size() ; j++){
			index = y_sorted.at(j).first;
			index_rot = x_sorted.at(j).first; //  POUR PERIMETRE
///////////////////////////////////////////
			if(dx_rot > 0 && is_in_a_set_ver_sweep(index_rot, rect_ids_tracker_x_rot)){
				if(not_in_a_set_hor_sweep(index_rot, rect_ids_tracker_y_rot)){
					//std::cout << "entry : " << x_sorted.at(j).first << " " << is_void_tracker.at(x_sorted.at(j).first) << std::endl;
					if(is_void_tracker.at(index_rot) == 1) {
						if(cnt_swp_hor_rot > 0 && void_count_rot == 0){ // new
							perimeter += dx_rot; // new
							//std::cout << "Add dx" << std::endl;
						}
						void_count_rot++;
					} else{
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0) {
							perimeter += dx_rot; // new
							//std::cout << "Add dx" << std::endl;
							y1_rot = x_sorted.at(j);
						}
						cnt_swp_hor_rot++;
					}
				} else{
					//std::cout << "exit :  " << x_sorted.at(j).first << " " << is_void_tracker.at(x_sorted.at(j).first)  << std::endl;
					if(is_void_tracker.at(index_rot) == 1){
						void_count_rot--;
						if(void_count_rot == 0 && cnt_swp_hor_rot > 0){
							y1_rot = x_sorted.at(j);
							perimeter += dx_rot; // new
							//std::cout << "Add dx" << std::endl;
							//printf("EXIT VOID DX ROT\n");
						}
					} else{
						cnt_swp_hor_rot--;
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0){
							perimeter += dx_rot;
							//std::cout << "Add dx" << std::endl;
						}
					}
				}
				rect_ids_tracker_y_rot.at(x_sorted.at(j).first) *= -1;
			}

/////////////////////////////////
			if(dx > 0 && is_in_a_set_ver_sweep(index, rect_ids_tracker_x)){
				if(not_in_a_set_hor_sweep(index, rect_ids_tracker_y)){
				//	std::cout << "entry : " << y_sorted.at(j).first <<std::endl;
					if(is_void_tracker.at(index) == 1) {
						if(cnt_swp_hor > 0 && void_count == 0){
							perimeter += dx;
							dy += y_sorted.at(j).second - y1.second;
				//			std::cout << "Add dx" << std::endl;
						}
						void_count++;
					} else{
						if(cnt_swp_hor == 0 && void_count == 0) {
				//				std::cout << "Add dx" << std::endl;
								perimeter += dx;
								y1 = y_sorted.at(j);
						}
						cnt_swp_hor++;
					}
				} else{
				//	std::cout << "exit : " << y_sorted.at(j).first  << std::endl;
					if(is_void_tracker.at(index) == 1){
						void_count--;
						if(void_count == 0 && cnt_swp_hor > 0){
							y1 = y_sorted.at(j);
				//			std::cout << "Add dx" << std::endl;
							perimeter += dx;
						}
					} else{
						cnt_swp_hor--;
						if(cnt_swp_hor == 0 && void_count == 0){
				//			std::cout << "Add dx" << std::endl;
							dy +=  y_sorted.at(j).second - y1.second;
							perimeter += dx;
						}
					}
				}
				rect_ids_tracker_y.at(y_sorted.at(j).first) *= -1;
			}
		}
		if(dx > 0) area += dx * dy;
	//	printf("---------------------\n");
	}

	printf("aire : %f\n", area);
	printf("perimetre : %f\n", perimeter);
	return 0;
}
