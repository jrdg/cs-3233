#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Rectangle {
	int id;
	std::pair<double,double> upper_left;
	std::pair<double,double> upper_right;
	std::pair<double,double> lower_left;
	std::pair<double,double> lower_right;
};

struct Three_uplet{
	int first;    // id
	double second;// x or y (x_sorted = x  ---  y_sorted = y)
	double third; // x or y (x_sorted = y  ---  y_sorted = x)
	int is_void;
	int is_entry;
};

struct Rectangle gen_rect_by_mid(
	int id,
	double x,
	double y,
	double base,
	double height
) {
	return {   
			   id,
			   std::pair<double,double>(x - base / 2, y + height / 2),
			   std::pair<double,double>(x + base / 2, y + height / 2), 
			   std::pair<double,double>(x - base / 2, y - height / 2), 
		       std::pair<double,double>(x + base / 2, y - height / 2)
		   };
}

bool xy_sort (
	struct Three_uplet p1,
	struct Three_uplet p2
) {
	if(p1.second < p2.second) return true;
	else if(p1.second == p2.second){
		if(p1.is_entry){
			if(p1.is_void){
				return true;
			}
		}else{
			if(p1.is_void) 
				return false;
		} 
		if (p2.is_entry) {
			if(p2.is_void){
				return false;
			}
		}else {
			if(p2.is_void)
				return true;
		}
		if(p1.is_entry) return true;
		else if(p2.is_entry) return false;
		if(p1.third == p2.third) return p1.first < p2.first;
		return p1.third < p2.third;
	}
	return false;
}

void quicksort(
	std::vector<struct Three_uplet> & arr,
	int start, 
	int end
) {
	if(start >= end) return;
	int pivot = start;
	struct Three_uplet tmp;
	int i = start + 1;
	int j = end;
	while(i <= j) {
		if(xy_sort(arr[i], arr[j]) == 0){
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
		if(xy_sort(arr[i], arr[j]) == 1) i++;
		if(xy_sort(arr[i], arr[j]) == 0) j--;		
	}
	tmp = arr[j];
	arr[j] = arr[start];
	arr[start] = tmp;
	quicksort(arr, start, j-1);
	quicksort(arr, j+1, end);
}

int is_in_a_set_ver_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_x
) {
	return rect_ids_tracker_x.at(index) == 1;
}

int not_in_a_set_hor_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_y
) {
	return rect_ids_tracker_y.at(index) == -1;
}


int main (){
	char c;
	double x, y, b, h, perimeter = 0, area = 0, dx, dx_rot, dy, dy_rot;
	int rect_id = 0, cnt_swp_hor, cnt_swp_hor_rot, cnt_swp_ver, void_count, void_count_rot, index, index_rot, nb_gap_ver, nb_gap_ver_rot;
	std::vector<struct Three_uplet> x_sorted, y_sorted;
	std::vector<int> rect_ids_tracker_x, rect_ids_tracker_y, is_void_tracker;
	std::vector<struct Three_uplet> x_sorted_rot, y_sorted_rot;
	std::vector<int> rect_ids_tracker_x_rot, rect_ids_tracker_y_rot;
	std::cout << "DEBUT DU PROGRAMME" << std::endl;
	while(std::cin >> c >> x >> y >> b >> h ) {
		rect_ids_tracker_x.push_back(-1);
		rect_ids_tracker_y.push_back(-1);
		is_void_tracker.push_back(c == 'p' ? 0 : 1);
		struct Rectangle rect = gen_rect_by_mid(rect_id, x, y, b, h);
		x_sorted.push_back({rect_id, rect.upper_left.first, rect.upper_left.second, is_void_tracker.at(is_void_tracker.size()-1), 1});
		x_sorted.push_back({rect_id, rect.upper_right.first, rect.upper_right.second, is_void_tracker.at(is_void_tracker.size()-1), 0});
		y_sorted.push_back({rect_id, rect.upper_left.second, rect.upper_left.first, is_void_tracker.at(is_void_tracker.size()-1), 0});
		y_sorted.push_back({rect_id, rect.lower_right.second, rect.lower_right.first, is_void_tracker.at(is_void_tracker.size()-1) , 1});
		rect_id++;
	}
	std::cout << "APRES LA PREMIERE BOUCLE ET AVANT LE SORT" << std::endl;
	quicksort(x_sorted, 0, x_sorted.size()-1);
	quicksort(y_sorted, 0, y_sorted.size()-1);
	//std::sort (x_sorted.begin(), x_sorted.end(), xy_sort);
	//std::sort (y_sorted.begin(), y_sorted.end(), xy_sort);
	std::cout << "APRES LES SORT ET AVANT L'AFFECTION DES ACTIVE SET DE ROTATION" << std::endl;
	rect_ids_tracker_x_rot = rect_ids_tracker_y; //  POUR PERIMETRE
	rect_ids_tracker_y_rot = rect_ids_tracker_x; //  POUR PERIMETRE
	std::cout << "APRES L'AFFECTION DES ACTIVE SET DE ROTATION" << std::endl;
	for(st i = 0 ; i < x_sorted.size()-1 ; i++){
		struct Three_uplet x1 = x_sorted.at(i);
		struct Three_uplet x2 = x_sorted.at(i+1);
		struct Three_uplet x1_rot = y_sorted.at(i); //  POUR PERIMETRE
		struct Three_uplet x2_rot = y_sorted.at(i+1); //  POUR PERIMETRE
		struct Three_uplet y1, y1_rot; //  POUR PERIMETRE
		dx = x2.second - x1.second;
		dx_rot = x2_rot.second - x1_rot.second;// pour perimetre
		rect_ids_tracker_x.at(x1.first) *= -1;
		rect_ids_tracker_x_rot.at(x1_rot.first) *= -1; //  POUR PERIMETRE
		dy = cnt_swp_hor = void_count = 0;
		dy_rot = cnt_swp_hor_rot = void_count_rot = 0; //  POUR PERIMETRE
		std::cout << "x1 : "  << x1.second << " x2 : " << x2.second << std::endl;
		std::cout << "..............................." << std::endl;
		for(st j = 0 ; j < y_sorted.size() ; j++){
			index = y_sorted.at(j).first;
			index_rot = x_sorted.at(j).first; //  POUR PERIMETRE
			if(dx_rot > 0 && is_in_a_set_ver_sweep(index_rot, rect_ids_tracker_x_rot)){
				if(not_in_a_set_hor_sweep(index_rot, rect_ids_tracker_y_rot)){
					if(is_void_tracker.at(index_rot) == 1) {
						if(cnt_swp_hor_rot > 0 && void_count_rot == 0){ // new
							perimeter += dx_rot; // new
						}
						void_count_rot++;
					} else{
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0) {
							perimeter += dx_rot; // new
							y1_rot = x_sorted.at(j);
						}
						cnt_swp_hor_rot++;
					}
				} else{
					if(is_void_tracker.at(index_rot) == 1){
						void_count_rot--;
						if(void_count_rot == 0 && cnt_swp_hor_rot > 0){
							y1_rot = x_sorted.at(j);
							perimeter += dx_rot; // new
						}
					} else{
						cnt_swp_hor_rot--;
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0){
							perimeter += dx_rot;
						}
					}
				}
				rect_ids_tracker_y_rot.at(x_sorted.at(j).first) *= -1;
			}
			if(dx > 0 && is_in_a_set_ver_sweep(index, rect_ids_tracker_x)){
				if(not_in_a_set_hor_sweep(index, rect_ids_tracker_y)){
					if(is_void_tracker.at(index) == 1) {
						if(cnt_swp_hor > 0 && void_count == 0){
							perimeter += dx;
							dy += y_sorted.at(j).second - y1.second;
						}
						void_count++;
					} else{
						if(cnt_swp_hor == 0 && void_count == 0) {
								perimeter += dx;
								y1 = y_sorted.at(j);
						}
						cnt_swp_hor++;
					}
				} else{
					if(is_void_tracker.at(index) == 1){
						void_count--;
						if(void_count == 0 && cnt_swp_hor > 0){
							y1 = y_sorted.at(j);
							perimeter += dx;
						}
					} else{
						cnt_swp_hor--;
						if(cnt_swp_hor == 0 && void_count == 0){
							dy +=  y_sorted.at(j).second - y1.second;
							perimeter += dx;
						}
					}
				}
				rect_ids_tracker_y.at(y_sorted.at(j).first) *= -1;
			}
		}
		if(dx > 0) area += dx * dy;
	}
	printf("aire : %f\n", area);
	printf("perimetre : %f\n", perimeter);
	return 0;
}
