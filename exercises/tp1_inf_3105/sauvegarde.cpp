#include <bits/stdc++.h>
#include <iostream>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Event{
	int first; 
	double second;
	double third;
	int is_void;
	int is_entry;
};

bool xy_sort (
	struct Event p1,
	struct Event p2
) {
	if(p1.second < p2.second) return true;
	else if(p1.second == p2.second){
		if(p1.is_void){
			if(p1.is_entry) return true;
			else return false;
		}else if(p2.is_void){
			if(p2.is_entry) return false;
			else return true;
		}
		if(p1.is_entry) return true;
		else if(p2.is_entry) return false;
		if(p1.third == p2.third) return p1.first < p2.first;
		return p1.third < p2.third;
	}
	return false;
}

void swap(
	struct Event * a, 
	struct Event * b
) { 
    struct Event t = *a; 
    *a = *b; 
    *b = t; 
} 
  
int partition (
	std::vector<struct Event> &arr, 
	int low, 
	int high
) { 
    struct Event pivot = arr.at(high);
    int i = (low - 1);
    for (int j = low; j <= high- 1; j++) { 
        if (xy_sort(arr.at(j), pivot)) { 
            i++; 
            swap(&arr.at(i), &arr.at(j)); 
        } 
    } 
    swap(&arr.at(i + 1), &arr.at(high)); 
    return (i + 1); 
} 
  
void quickSort(
	std::vector<struct Event> &arr, 
	int low, 
	int high
) { 
    if (low < high) { 
        int pi = partition(arr, low, high); 
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
} 

int is_in_a_set_ver_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_x
) {
	return rect_ids_tracker_x.at(index) == 1;
}

int not_in_a_set_hor_sweep(
	int index,
	std::vector<int> & rect_ids_tracker_y
) {
	return rect_ids_tracker_y.at(index) == -1;
}


int main (){
	char c, p_or_n;
	double x, y, base, height, perimeter = 0, area = 0, dx, dx_rot, dy, dy_rot, l_side, r_side, u_side, b_side;
	int rect_id = 0, cnt_swp_hor, cnt_swp_hor_rot, cnt_swp_ver, void_count, void_count_rot, index, index_rot;
	std::vector<struct Event> x_events, y_events;
	std::vector<int> rect_ids_tracker_x, rect_ids_tracker_y, is_void_tracker;
	std::vector<struct Event> x_events_rot, y_events_rot;
	std::vector<int> rect_ids_tracker_x_rot, rect_ids_tracker_y_rot;
	while(std::cin >> c >> x >> y >> base >> height) {
		p_or_n = c == 'p' ? 0 : 1;
		l_side = x - base / 2;
		r_side = x + base / 2;
		u_side = y + height / 2;
		b_side = y - height / 2;
		rect_ids_tracker_x.push_back(-1);
		rect_ids_tracker_y.push_back(-1);
		is_void_tracker.push_back(p_or_n);
		x_events.push_back({rect_id, l_side, u_side, p_or_n, 1});
		x_events.push_back({rect_id, r_side, u_side, p_or_n, 0});
		y_events.push_back({rect_id, u_side, l_side, p_or_n, 0});
		y_events.push_back({rect_id, b_side, r_side, p_or_n, 1});
		rect_id++;
	}
	quickSort(x_events, 0, x_events.size()-1);
	quickSort(y_events, 0, y_events.size()-1);
	rect_ids_tracker_x_rot = rect_ids_tracker_y;
	rect_ids_tracker_y_rot = rect_ids_tracker_x;
	for(st i = 0 ; i < x_events.size()-1 ; i++){
		struct Event y1, y1_rot;
		dx = x_events.at(i+1).second - x_events.at(i).second;
		dx_rot = y_events.at(i+1).second - y_events.at(i).second;
		rect_ids_tracker_x.at(x_events.at(i).first) *= -1;
		rect_ids_tracker_x_rot.at(y_events.at(i).first) *= -1;
		dy = cnt_swp_hor = void_count = 0;
		dy_rot = cnt_swp_hor_rot = void_count_rot = 0;
		for(st j = 0 ; j < y_events.size() ; j++){
			index = y_events.at(j).first;
			index_rot = x_events.at(j).first;
			if(dx_rot > 0 && is_in_a_set_ver_sweep(index_rot, rect_ids_tracker_x_rot)){
				if(not_in_a_set_hor_sweep(index_rot, rect_ids_tracker_y_rot)){
					if(is_void_tracker.at(index_rot) == 1) {
						if(cnt_swp_hor_rot > 0 && void_count_rot == 0)
							perimeter += dx_rot;
						void_count_rot++;
					} else{
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0) {
							perimeter += dx_rot;
							y1_rot = x_events.at(j);
						}
						cnt_swp_hor_rot++;
					}
				} else{
					if(is_void_tracker.at(index_rot) == 1){
						void_count_rot--;
						if(void_count_rot == 0 && cnt_swp_hor_rot > 0){
							y1_rot = x_events.at(j);
							perimeter += dx_rot;
						}
					} else{
						cnt_swp_hor_rot--;
						if(cnt_swp_hor_rot == 0 && void_count_rot == 0){
							perimeter += dx_rot;
						}
					}
				}
				rect_ids_tracker_y_rot.at(x_events.at(j).first) *= -1;
			}
			if(dx > 0 && is_in_a_set_ver_sweep(index, rect_ids_tracker_x)){
				if(not_in_a_set_hor_sweep(index, rect_ids_tracker_y)){
					if(is_void_tracker.at(index) == 1) {
						if(cnt_swp_hor > 0 && void_count == 0){
							perimeter += dx;
							dy += y_events.at(j).second - y1.second;
						}
						void_count++;
					} else{
						if(cnt_swp_hor == 0 && void_count == 0) {
								perimeter += dx;
								y1 = y_events.at(j);
						}
						cnt_swp_hor++;
					}
				} else{
					if(is_void_tracker.at(index) == 1){
						void_count--;
						if(void_count == 0 && cnt_swp_hor > 0){
							y1 = y_events.at(j);
							perimeter += dx;
						}
					} else{
						cnt_swp_hor--;
						if(cnt_swp_hor == 0 && void_count == 0){
							dy +=  y_events.at(j).second - y1.second;
							perimeter += dx;
						}
					}
				}
				rect_ids_tracker_y.at(y_events.at(j).first) *= -1;
			}
		}
		if(dx > 0) area += dx * dy;
	}
	printf("aire : %f\n", area);
	printf("perimetre : %f\n", perimeter);
	return 0;
}
