#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

/*
 * Complete the 'strangelyCompatible' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING_ARRAY students as parameter.
 */

long long strangelyCompatible(vector<string> students) {
    vector<string> uniq_profile;  
    std::map<string,long long> count_profil;
    std::map<string,long long> count3_profil;
    std::map<string, long long>::iterator it;
    long long to_supress = 0;
    long long total = 0;
    string tmp;
    
    
    for(size_t i = 0 ; i < students.size() ; i++){
        count_profil[students[i]]++;
    }
    
    for ( it = count_profil.begin(); it != count_profil.end(); it++ ) {
        for(size_t i = 0 ; i < it->first.length() ; i++){
            tmp = it->first;
            tmp[i] = '?';
            count3_profil[tmp] += it->second;
        }
        to_supress += it->first.length() * ( ( it->second * it->second - it->second)  / 2 );

    }
    
    for ( it = count3_profil.begin(); it != count3_profil.end(); it++ ) {
        total += ( it->second * it->second - it->second)  / 2;
    }
   
    return total - to_supress;
}

int main()
{
    //ofstream fout(getenv("OUTPUT_PATH"));

    string n_temp;
    getline(cin, n_temp);

    int n = stoi(ltrim(rtrim(n_temp)));

    string m_temp;
    getline(cin, m_temp);

    int m = stoi(ltrim(rtrim(m_temp)));

    vector<string> students(n);

    for (int i = 0; i < n; i++) {
        string students_item;
        getline(cin, students_item);

        students[i] = students_item;
    }

    long long result = strangelyCompatible(students);

    std::cout << "reponse : " << result << std::endl;;

    //fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end()
    );

    return s;
}
