#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct A{
	int value;
};

class Action {
	public:
		virtual void manger(){

		}
};

class Personne : public Action{
	private:
		int age;
		std::string nom;
	public:
		Personne(int _age, std::string _nom): age(_age), nom(_nom){

		}
	
		std::string & get_name(){
			return nom;
		}

		void set_name(std::string name){ 
			nom = name;
		}

		virtual void manger(){
			std::cout << nom << " est entrain de manger" << std::endl;
		}
};

int main (){
	std::string name = std::string("jordan");
	Personne * p = new Personne(12, name);
	p->set_name(std::string("charles"));
	p->manger();
	delete p;
	return 0;
}

