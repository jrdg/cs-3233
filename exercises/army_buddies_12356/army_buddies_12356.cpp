#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>

/*
 *
 * TITRE : UVA 13256 - Army Buddies
 *
 * URL : https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=3778
 *
 * EXPLICATION DU TRUC POUR SAUVER DU TEMPS :
 *
 * Les indices des buddies gauche et droite de
 * chaque soldat sont stocker dans leur tableau
 * respectif sois buddies_left ou buddies_right.
 *
 * Pour acceder au buddy gauche ou droite d'un
 * soldat il faut consulter le tableau respectif
 * de cette direction a l'indice de ce soldat.
 *
 * EXEMPLE :
 *
 * Le symbol "-" represente un soldat mort.
 * Le symbol "*" signifie 
 *
 * soldats = {0, -, -, -, -, 5, 6, 7, 8, 9}
 *            
 *            0  1  2  3  4  5  6  7  8  9
 * b_right = {5, -, -, -, -, 6, 7, 8, 9, *}
 *
 *            0  1  2  3  4  5  6  7  8  9
 * b_left  = {*, -, -, -, -, 0, 5, 6, 7, 8}
 *
 * MISE EN SITUATION :
 *
 * Pour savoir les buddy du soldat a l'indice 5
 * il faut consulter b_right[5] et b_left[5].
 * 
 * Donc :
 *
 * L'ami a gauche du soldat 5 est: b_left[5]  = 0
 * L'ami a droite du soldat 5 est: b_right[5] = 6
 *
 * Pour le probleme il est mentionner que les
 * soldats sont numeroter de 1..S ou S est le
 * nombre soldat. Il faut donc lors de
 * l'affichage du couple d'amis de nouveau soldat
 * afficher b_left[n]+1 et b_right[n]+1.
 *
 * Donc l'algorithme simule une liste doublement
 * chaine. Chaque b_left[i] correspond au
 * node.left du soldat[i] et chaque b_right[i]
 * correspond au node.right du soldat[i].
 *
 * EXPLICATIONS DES VARIABLES UTILISE :
 *
 * s       = Nombre de soldat
 * b       = Nombre de rapport de deces
 * l       = Limite de l'interval de gauche
 * r       = Limite de l'interval de droite
 * b_left  = Position de l'ami de gauche de 
 *           chaque soldat
 * b_right = Positionn de l'ami de droite de
 *           chaque soldat.
 * */

#define MAX_S 100000

int main (){

    int s,b,l,r;
    int b_left[100010], b_right[100010];

    scanf("%d%d", &s, &b);

    while(s != 0){

        /**
         * On fill la position de l'ami de gauche
         * et de l'ami de droite de chaque soldat
         * dans leur tableau respectif
         */
        for(size_t i = 0 ; i < s ; i++){
            b_left[i] = i-1;
            b_right[i] = i+1;
        }

        /**
         * On indique que l'ami de gauche du 
         * premier soldat ainsi que l'ami de 
         * droite du dernier soldat n'existent 
         * pas.
         */
        b_left[0] = -1;
        b_right[s-1] = -1;

        // On itere les rapport de deces 1 par 1
        while(b--){

            /** 
             * On scan la limite de gauche et la
             * limite de droite de l'interval du 
             * rapport de deces presentement
             * iterer.
             */
            scanf("%d%d", &l, &r);

            /**
             * On decremente les deux soldat pour
             * ne pas a avoir a faire +1 a tous
             * les tests concernant ces 2
             * variables.
             */
            l--;r--;
            

            /**
             * Si le soldat l a un ami a gauche,
             * l'ami de droite de l'ami de gauche
             * du soldat l est maintenant l'ami 
             * de droite du soldat r
             */
            if(b_left[l] != -1)
                b_right[b_left[l]] = b_right[r];

            /**
             * L'ami de gauche du soldat r est
             * maintenant l'ami de gauche du 
             * soldat l
             */
            if(b_right[r] != -1)
                b_left[b_right[r]] = b_left[l];

            
            if(b_left[l] == -1) printf("*");
            else printf("%d", b_left[l]+1);
            if(b_right[r] == -1) printf(" *\n");
            else printf(" %d\n", b_right[r] + 1);
        }

        printf("-\n");
        scanf("%d%d", &s, &b);
    }



    return 0;
}
