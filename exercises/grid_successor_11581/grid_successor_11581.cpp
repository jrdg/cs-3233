#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>

#define ui unsigned int
#define ull unigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){

    int n, s, fc, a[9], t0, t1, t2, t3, t4, t5, t6, t7, t8;

    scanf("%d", &n);

    while(n--) {
        fc = 0;
        s = 0;

        for(st i = 0 ; i < 9; i++){
            scanf("%1d", &a[i]);
            s += a[i];
        } 

        if(s != 0) {

            do {
                t0 = ( a[1] + a[3] ) % 2;
                t1 = ( a[0] + a[2] + a[4] ) % 2;
                t2 = ( a[1] + a[5] ) % 2;
                t3 = ( a[0] + a[4] + a[6] ) % 2;
                t4 = ( a[1] + a[3] + a[5] + a[7] ) % 2;
                t5 = ( a[2] + a[4] + a[8] ) % 2;
                t6 = ( a[3] + a[7] ) % 2;
                t7 = ( a[4] + a[6] + a[8] ) % 2;
                t8 = ( a[5] + a[7] ) % 2;
                a[0] = t0;
                a[1] = t1;
                a[2] = t2;
                a[3] = t3;
                a[4] = t4;
                a[5] = t5;
                a[6] = t6;
                a[7] = t7;
                a[8] = t8;
                fc++;

            } while(t0 + t1 + t2 + t3 + t4 +
                    t5 + t6 + t7 + t8 != 0);

        }

        printf("%d\n",fc-1);
    } 

    return 0;
}

