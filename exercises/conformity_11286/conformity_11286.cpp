#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <map>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short

int main (){

    int n, max, c;
    ull t, a[5];
    std::map<ull,int> map;
    std::map<ull,int>::iterator it;

    scanf("%d", &n);

    /**
     * Itere chaque teste
     */
    while(n != 0) {

        /**
         * reinitialise le max a 0
         */
        max = 0;

        /**
         * Itere chaque freshman
         */
        while(n--){
            /**
             * On scan les 5 cours du freshman 
             */
            scanf("%llu%llu%llu%llu%llu", 
                     &a[0], &a[1], &a[2],
                     &a[3], &a[4]);

            /**
             * On sort le tableau de cour
             */
            std::sort(a, a + 5);

            /**
             * On calcule la cle de la map
             */
            t = a[0] * 1000000000000 +
                a[1] * 1000000000    +
                a[2] * 1000000       +
                a[3] * 1000          +
                a[4];

            /**
             * On inncremennt la valeur calculer
             */
            map[t]++;

            /**
             * On regarde si la combinaisonn a
             * pris le dessu sur l'ancienne 
             * combinaison maximal
             */
            max = std::max(max, map[t]);
        }
        
        /**
         * On met le compteur a 0
         */
        c = 0;

        /**
         * On itere toute la map puis 
         * quand qu'une valeur a un nombre
         * de freshman egale a max qui a choisi
         * ces cours on ajoute ce nombre au
         * compteur.
         */
        it = map.begin();
        while(it != map.end()){
            if(it->second == max) c+=it->second;
            it++;
        }
        printf("%d\n",c);
        scanf("%d", &n);
        map.clear();
    }

    return 0;
}

