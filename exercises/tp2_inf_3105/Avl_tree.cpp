/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include "Avl_tree.h"

/**
	Instancie un arbre avl completement vide.
*/
Avl_tree::Avl_tree(
	void
) {
	racine = NULL;
}
		
/**
	Instancie un Avl_tree avec un noeud de depart

	@param noeud Le noeud de depart de l'avl_tree.
*/
Avl_tree::Avl_tree(
	Noeud & noeud
) {
	racine = &noeud;
}

/**
	Ajoute un nœud dans l’arbre et le
	rééquilibre. Devra aussi maintenir l’intégrité des a. Utilisé pour la 
	commande insertion
	
	@param clef La valeur de clef
	@param valeur La valeur de noeud
*/
void Avl_tree::inserer( 
	double clef, 
	double valeur 
) {
	inserer(this->racine, clef, valeur);
}

/**
	Retourne une liste de tous les pair cle valeur ayant 
	une cle <= a x

	@param x La valeur a comparer avec les cles

	@return  Un vecteur de pair
*/		
std::vector< std::pair< double, double > > Avl_tree::jusqua(
	double x
) {
	std::vector< std::pair< double, double > > liste;
	ajouter_liste(liste, racine, x);
	return liste;
}
		
/**
	Inserer les pair cle valeur dans liste ou les cle son <= x

	@param liste Le vecteur dans lequel les pair cle valeur seront inserer
	@param noeud Une reference sur un pointeur de de type Noeud
	@param x La valeur sur laquel les cle seront compare.
*/
void Avl_tree::ajouter_liste(
	std::vector< std::pair< double, double > > & liste,
	Noeud *& noeud,
	double x
) {
	if(noeud != NULL){
		ajouter_liste(liste, noeud->get_gauche(), x);
		if(noeud->get_clef() <= x){
			liste.push_back(std::make_pair(noeud->get_clef(), 
										   noeud->get_valeur()));
		}
		if(noeud->get_clef() < x)
			ajouter_liste(liste, noeud->get_droite(), x);
	}
}	

	
/**
	Insere un noeud dans la structure avec comme valeur de cle : cle et valeur : 
	valeur

	@param noeud le noeud sur lequel l'appel recursif fait son action
	@param clef la valeur de la clef
	@param valeur la valeur de la valeur

	@return vrai si equilibre == 1 ou -1 ou si le noeud a 
			ete inserer ou si le noeud est deja dans l'arbre
*/	
bool Avl_tree::inserer(
	Noeud *& noeud, 
	double clef, 
	double valeur
) {
	if(noeud == NULL){
		noeud = new Noeud(clef, valeur);
		return true;	
	} 
			
	if(clef < noeud->get_clef()){
		if(valeur > noeud->get_a())
			noeud->set_a(valeur);
		if(inserer(noeud->get_gauche(), clef, valeur)){
			noeud->set_equilibre(noeud->get_equilibre() + 1);
			if(noeud->get_equilibre() == 0)
				return false;
			if(noeud->get_equilibre() == 1)
				return true;
           	if(noeud->get_gauche()->get_equilibre() == -1)
              		rotationDroiteGauche(noeud->get_gauche());
       		rotationGaucheDroite(noeud);
		} 
		return false;
	} else if(clef > noeud->get_clef()) {
		if(valeur > noeud->get_a())
			noeud->set_a(valeur);
		if(inserer(noeud->get_droite(), clef, valeur)){
			noeud->set_equilibre(noeud->get_equilibre() - 1);
			if(noeud->get_equilibre() == 0)
				return false;
			else if(noeud->get_equilibre() == -1)
				return true;
			else if(noeud->get_droite()->get_equilibre() == 1)
				rotationGaucheDroite(noeud->get_droite());
			rotationDroiteGauche(noeud);
		} 
		return false;
	}
	return true;
}


/**
	Retourne la valeur de a placé à la racine. Utilisé pour répondre à la 
	commande max.
	
	@param clef La valeur de clef rechercher pour renvoyer la valeur de son a.
*/
double Avl_tree::maxima(
	void
) {
	assert(racine != NULL);
	return racine->get_a();
}

/**
	retourne la valeur a du noeud ayant comme valeur de clef : x.

	@param x valeur de la clef
*/
double Avl_tree::maxima(
	double x
) {
	return trouver_max(racine, x);	
}

/**
	Trouve la valeur a du noeud ayant comme valuer de clef : x

	@param node le noeud sur lequel la clef est presentement verifier
	@param x clef rechercher
*/
double Avl_tree::trouver_max(
	Noeud *& node,
	double x
) {
	double tr;
	assert(node != NULL);
	if(x == node->get_clef())
		tr = node->get_a();
	if(x < node->get_clef())
		tr = trouver_max(node->get_gauche(), x);
	else 
		tr = trouver_max(node->get_droite(), x);
	return tr;
}

/**
	Retourne la valeur maximale des valeurs associées aux clefs
	plus petites ou égales à x. Utilisé pour la commande donne et appartient.

	@param x La valeur de comparaison des clefs.

	@return un double.
*/		
double Avl_tree::aGauche( 
	double x 
) {
	return aGauche(racine, x);
}

/**
	Retourne la valeur maximal des valeurs associees au clefs
	plus petites ou egale a x. Utilise pour la commande donne et appartient.

	@param noeud le noeud presentement iterer
	@param x La valeur a comparer

	@return Un double.
*/
double Avl_tree::aGauche(
	Noeud *& noeud, 
	double x
) {
	double tr;
	if(noeud == NULL)
		tr = -INFINITY;
	else if(x < noeud->get_clef())
		tr =  aGauche(noeud->get_gauche(), x);
	else
		tr = std::max(noeud->get_gauche() != NULL &&
						noeud->get_gauche()->get_a() > noeud->get_valeur() ? 
						noeud->get_gauche()->get_a() : noeud->get_valeur() ,
					    aGauche(noeud->get_droite(), x));
	return tr;
}

/**
	Acces a la tete de l'arbre avl
	
	@return Une reference sur le pointeur pointant 
		    sur le noeud en guise de tete de l'arbre avl.
*/
Noeud *& Avl_tree::get_head(
	void
) {
	return this->racine;
}

/**
	Effectue une rotation de gauche a droite pour reequilibre l'arbre avl.

	@param racinesousarbre Le noeud necessistant un reequilibrage.
*/
void Avl_tree::rotationGaucheDroite(
	Noeud *& racinesousarbre
) {
   	Noeud *temp = racinesousarbre->get_gauche();
   	int  ea = temp->get_equilibre();
   	int  eb = racinesousarbre->get_equilibre();
   	int  neb = -(ea > 0 ? ea : 0) - 1 + eb;
   	int  nea = ea + (neb < 0 ? neb : 0) - 1;
	double lra = temp->get_droite() == NULL ? 
		 	 racinesousarbre->get_valeur() :
		 	 temp->get_droite()->get_a();
	double ra = racinesousarbre->get_droite() == NULL ? 
			racinesousarbre->get_valeur() :
		    racinesousarbre->get_droite()->get_a();
	temp->set_a(racinesousarbre->get_a());
	racinesousarbre->set_a(lra > ra ? lra : ra);
   	temp->set_equilibre(nea);
   	racinesousarbre->set_equilibre(neb);
   	racinesousarbre->set_gauche(temp->get_droite());
  	temp->set_droite(racinesousarbre);
  	racinesousarbre = temp;
}


/**
	Effectue une rotation de droite a gauche pour reequilibre l'arbre avl.

	@param racinesousarbre Le noeud necessistant un reequilibrage.
*/
void Avl_tree::rotationDroiteGauche(
	Noeud *& racinesousarbre
) {
  	Noeud *temp = racinesousarbre->get_droite();
  	int  ea = temp->get_equilibre();
   	int  eb = racinesousarbre->get_equilibre();
   	int  neb = -(ea > 0 ? ea : 0) - 1 + eb;
  	int  nea = ea + (neb < 0 ? neb : 0) - 1;
	double rla = temp->get_gauche() == NULL ? 
			 	 racinesousarbre->get_valeur() :
			     temp->get_gauche()->get_a();
	double la = racinesousarbre->get_gauche() == NULL ? 
				racinesousarbre->get_valeur() :
			    racinesousarbre->get_gauche()->get_a();
	temp->set_a(racinesousarbre->get_a());
	racinesousarbre->set_a(rla > la ? rla : la);
   	temp->set_equilibre(nea);
   	racinesousarbre->set_equilibre(neb);
   	racinesousarbre->set_droite(temp->get_gauche());
   	temp->set_gauche(racinesousarbre);
	racinesousarbre = temp;
}


