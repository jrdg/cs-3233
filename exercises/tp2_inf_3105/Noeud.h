/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include <iostream>
#include <bits/stdc++.h> 

class Noeud;

class Noeud {
	private:
		double clef;
		double valeur;
		double a;
		int equilibre;
		Noeud * gauche;
		Noeud * droite;
	public:

		/**
			Instancie un Noeud avec une clef et une valeur.
			Le A = valeur et l'equilibre = 0.

			@param clef la valeur de la clef
			@param valeur la valeur de la valeur
		*/
		Noeud( double clef, double valeur );
		
		/**
			Acces au noeud de gauche du noeud courrant.
			
			@return Une reference sur le pointeur du noeud de gauche.
		*/
		Noeud *& get_gauche( void );	
		
		/**
			Acces au noeud de droite du noeud courrant.
			
			@return Une reference sur le pointeur du noeud de droite.
		*/
		Noeud *& get_droite( void );

		/**
			Acces a la valeur du A de ce noeud.
		
			@return Un double representant le A.
		*/
		double get_a( void );
		
		/**
			Acces a la valeur de ce noeud

			@return un double representant la valeur de ce noeud.
		*/
		double get_valeur( void );

		/**
			Acces a la valeur de la clef de ce noeud.
		
			@return un double representant la valeur de la clef.
		*/
		double get_clef( void );

		/**
			Acces a la valeur de l'equilibre de ce noeud.

			@return un entier representant la valeur de l'equilibre de ce noeud.
		*/
		int get_equilibre( void );

		/**
			Modifie la valeur du A de ce noeud

			@param a la nouvelle valeur que prendra le a			
		*/
		void set_a( double a );

		/**
			modifie la valeur de la valeur du noeud.
			
			@param valeur la nouvelle valeur
		*/
		void set_valeur( double valeur );

		/**
			modifie la valeur de la clef

			@param clef la nouvelle valeur de la clef 
		*/
		void set_clef( double clef );

		/**
			Modifie le noeud de gauche

			@param noeud le nouveau noeud de gauche
		*/
		void set_gauche( Noeud *& noeud );

		/**
			Modifie le noeud de droite

			@param noeud le nouveau noeud de droite
		*/
		void set_droite( Noeud *& noeud );

		/**
			Modifie l'equilibre du noeud

			@param equilibre la nouvelle equilibre du noeud.
		*/
		void set_equilibre( int equilibre );
};

