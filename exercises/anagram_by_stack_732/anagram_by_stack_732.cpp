#include <bits/stdc++.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))

struct Node {
	std::string str;
	std::vector<struct Node> links;
	std::stack<char> stack;
};

void fill(std::vector<char> source, int s_i,
          std::vector<char> target, int t_i,
		  struct Node node
){
	while(s_i < source.size()){
		node.str.push_back('I');
		node.stack.push(source.at(s_i));
		if(source[s_i] == target[t_i]){
			struct Node new_node;
			new_node.str.push_back('O');
			fill(source, s_i + 1, target, t_i + 1, new_node);
			node.links.push_back(new_node);
		}
		s_i++;	
	}

}

void print_node(struct Node node){
	for(st i = 0 ; i < node.links.size() ; i++){
		std::cout << node.str;
		print_node(node.links.at(i));
		printf("\n");
	}
}

int main (){
	char target[1000];
	char source[1000];
	
	while(scanf("%s\n%s\n",source , target) != EOF){
		printf("source : %s\n", source);
		printf("target : %s\n------\n", target);
//		std::vector<char> source_vec (source, source + sizeof(source) / sizeof(char) );
//		std::vector<char> target_vec (target, target + sizeof(target) / sizeof(char) );
		struct Node node;
//		fill(source_vec, 0, target_vec, 0, node);

		
		print_node(node);
	}
	return 0;
}

