#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <bitset>
#include <algorithm>
#include <math.h>

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short


/**
 * Le but de ce probleme etait
 * de splitter un nombre binaire en 
 * 2 de la maniere suivante :
 *
 * N = Nombre binaire a splitter
 * A = Premier nombre splitter
 * B = Deuxieme resultat du nombre splitter 
 *
 * A chaque `1` de N on set en alternance
 * 1 au meme index dans A et B.
 *
 * EXEMPLE :
 *
 * N = 1 0 1 0 1 1 0 1
 *     A   B   A B   A
 *
 * A = 1 0 0 0 1 0 0 1
 * B = 0 0 1 0 0 1 0 0
 *
 * Donc l'algorithme consiste a ajouter
 * cette puissance de deux a notre nombre 
 * A ou B a chaque iteration.
 *
 * A = 10000000 + 1000 + 1 = 10001001
 * B = 100000   + 100      =   100100
 */

int add(int i, int n){
    int t;
    return (t = (n & 1 << i)) 
           ? t : 0; 
}


int main (){
    ll n , n1, n2, l, f, t;

    scanf("%lld", &n);
    
    while(n != 0) {
        n1 = 0; n2 = 0; f = 0;
        l = (int)ceil(log2(n));
        for(st i = 0 ; i <= l ; i++) {
            if((t = add(i, n)) > 0) {
                if (f) {n2 += t; f = 0;}
                else {n1 += t ; f = 1;}
            }
        }
        printf("%lld %lld\n", n1, n2);
        scanf("%lld", &n);
    }

	return 0;
}

