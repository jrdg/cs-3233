# Superflu

##### Combien d'operation peu effectuer un ordinateur moderne en quelques secondes ?

`1 000 000`

##### Si une question necessite de manipuler un chiffre plus grand que `2^64 - 1` quel langage sommes-nous mieux d'utiliser enntre `c++` et `java` ?

`java` a cause de la classe big integer

##### Quelle est le combo de foncntions le plus rapide entre `cin et cout` et `scanf et printf` ?

`scanf` et `printf` 

##### Est ce que scanf et printf supporte un peu de fonctionnnalite regex ?

oui

##### Si on a besoin d'unn array conntenant seulement des boolean qu'est que sommes nous mieux d'utiliser ?

Un `bitset` array 

##### Calculer 2 nombre choisi au hazard de decimal en binaire et le contraire ennsuite.

# Big O Notation

##### Quand on calcule le big o notation est ce en quelle uniter de mesure est le resultat (e.g : n logn) ?

Il sera en `octet` ou en `byte`

##### Si on a une boucle avec un nombre `k` de sous boucle de n iteration chaque quelle est la complexite ?

`O(n^k)`

##### Une fonction recursive avec un nombre `b` d'appel recursif par niveau de `L` niveau ?

`O(b^L)`

##### Quelle est le big o notation de l'enumeration des permutationn ?

`n!`

##### Comment est ce que le shiftage de bit fonctionne ?

A chaque fois que l'on shift de 1 bit a gauche on multiplie par 2 :

```c++
int nb = 8
nb <<= 1
std::cout << nb << std::endl // Le nombre va avoir une valeur de 16
nb <<= 2
std::cout << 64 << std::endl // le nombre va avoir une valeur de 64 car cette fois si on a shifter de deux.
```

A chaque fois que l'ont shife a droite on divise par deux.

```c++
int nb = 16
nb >>= 1
std::cout << nb << std::endl // Le nombre va avoir une valeur de 8
nb >>= 2
std::cout << 64 << std::endl // le nombre va avoir une valeur de 2 car cette fois si on a shifter de deux.
```


# Questionn metrique

##### Qu'est ce que donne 2^10 (et sa representation approximative puissance de 10) ?

`1024`

sa represenntation approximative est : `10^3`

##### Qu'est ce que donne 2^20 et sa representation approximative en puissance de 10 ?

`1 048 576` 
    
et sa representation approximative est : `10^6` 

##### Quelle est la limit d'un `Int` 32-bit signed ?

`2^31`

##### Quelle est la limite d'un `Long long` ?

`2^63`

##### A partir de quelle nombre faut-il utiliser les `big integer` ?

`2^64`

##### En moyenne combien d'operation par 3 secondes  peu effectuer un processseur datant de 2013 ?

`100M (MB)` ou `10^8`


# Algorithme de tri 

##### Expliquer les etapes pour effectuer l'algoritme `counting sort` ?

si on a un tableau de `n` elemennt au depart.

1. trouver le range : le range c'est la valeur minimal et maximal..
2. creer un tableau contenant les indice du minnimum range to maximum range
3. placer a chaque indice i du tableau creer en 2 le nombre de valeur ayant comme valeur i dans le tableau qu'on veu trier.
4. ajouter au tableau creer en 2 la valeur qui precede cette case. (si on ajoute 1 a la case 2 alors la case 3 se verra ajouter la case 2 ainsi que la case 1).
5. creer un tableau de n element.
6. iterer le tableau de depart pour chaque valeur aller a son indice i dans le tableau creer en 2 puis mettre la valeur du premier tableau a l'indice (valeur du tableau 2 a l'idnice i) -1 dans le nouveau tableau.
   et ce jusqua la fin de literation du premier tableau.

##### Donner un exemple du counting sort ?

Tableau de depart (A) = [1 4 1 2 7 5 2]

.1 trouver le range :

Range = [0-9]

2. creer un tableau contenant les inndices du range

[                 ] tableau de 10 case (0-9) (B)

3. on a place a chaque indice i du tableau `B` le nombre de valeur de cette indice dans le tableau `A`

  0 1 2 3 4 5 6 7 8 9
[ 0 2 2 0 1 1 0 1 0 0 ]

4. On ajoute case b(i) la case b(i-1) 

  0 1 2 3 4 5 6 7 8 9
[ 0 2 4 4 5 6 6 7 7 7  ]

5. on Creer unn tableau avec le nombre d'elemennt du tableau `A` donc un tableau de 7 element (C) :

6. On itere le tableau `A` et on place dans le tableau `C[B[A[i]]-1]` la valeur a A[i] Eensuite on decremennt la valeur a B[A[i]].

On a fini donnc avec 
  0 1 2 3 4 5 6 
[ 1 1 2 2 4 5 7 ]

##### Qu'est ce que le radix sort ?

Le radix sort utilise le counting sort comme sub routine. En fait on va a chaque iteration trier les nnombre en fonctionn de leur dernier nombre,avant dernier nombre etc...

##### Qu'est ce que le bucket sort ?

1. On creer unn array de array SOIS A[i][j] ou i = le nombre d'element dans le array initial.
2. On itere notre array inntial et on place chaque element a l'indice `n*arr[i]`
3. on sort chaque sous array individuellemennt
4. on replace les bucket dans l'array original en ordre croissant.


# BITSET object c++

##### Qu'est ce que fais la classe Bitset ?

C'est un array de bit qu'on utilise a la place d'utiliser un array de boolean car le bitset utilise beaucoup moin de memoire.

##### Commennt peut on initialiser un bitset ?

Il y a 3 methode pour initialiser un bit set.

La premiere c'est d'initialiser le bitset avec une valeur de 0 a chaque index.

```c++
bitset<nb_bit> mon_bit_set;
```

La deuxieme c'est d'initialiser le bitset avec une valeur hexadecimal (0xfff) ou en decimal directemennt (32)

```c++
std::bitset<16> mon_bitset(108);
```

La troisieme est d'initialiser la valeur avec un string binaire :

```
std::bitset<16> mon_bitset(std::string("100000"));
```

##### Qu'est ce que signifie l'operateur `|=` ?

`C'est le bitwise OR`

##### Qu'est ce que le `bitwise OR` ?

C'est la comparaison de deux chainne binaire. Lorsque effectuer la comparaison donnerai une qui aura unne valeur de 1 
a chaque index ou une des deux chainnes ou les deux onnt un 1 comme valeur et 0 sinonn.


##### Qu'est ce que fais la ligne de code `3 |= 4 << 2` ?

ca effectue un `bitwise OR test` sur le chiffre `3` et le chiffre `2 shift 4` :

3      = `00000011`
4      = `00000100`
4 << 2 = `00010000`

Ensuite on effectue le bitwise operator 3 |= (4 << 2) =

```
  00000011
  00010000
----------
= 00010011
= 19
```

##### Comment faire pour turn on le nth bit d'une chaine binaire avec le bitwise or ?

si on a une variable `a` et que l'on veu turn on le bit `4` a `on` alors il faut faire :

```c++
a |= (1 << 4)
```

Cette ligne de code signifie compare la chainne binaire `a` avec la chainne binaire 1 (000001) shifter de 4 (000010000).

##### Quel est la syntax de l'operateur a << b ?

`a` : est le nombre de depart.
`b` : est le nombre de shift que l'on fait sur `a` 

##### Qu'est ce l'operation &= ?

c'est le `bitwise AND` operator 

##### Qu'est ce que fais le bitwise AND operator ?

Il compare deux chainne binaire et met a 1 tous les index au quelle les deux index des deux chaine sont a 1 sinon il met 0.

##### Commennt faire avec le bitwise AND pour savoir si dans unne chaine binaire un bit specifique = 1 ?

Il faut creer une chainne binaire ou seulemennt le bit specifique = 1 et comparer cette chainne avec un bitwise and avec la chainne sur laquelle le bit specifique est tester.

exemple


Si on a la chaine `1110011`. On veu savoir si le 2ieme bit = 1.
on creer la chainne `0000010` puis on les compare avec un bitwise and

```
1110011
0000010
-------
= 0000010 (ensuite on test si cette chaine est > 0 si oui alors le bit etais a 1 dans la chainne initial).
```

##### Comment utiliser le bitwise NOT operation pour mettre un bit specifique a 0 ?

Il faut comparer la chaine avec unne chainne dont tous les bit sont a 1 sauf le bit que lon veu supprimer avec loperateur bitwise and.

exemple 

```
A = 10001
B = ~(1 << 3) // 11011
A &= B        // 10001 & 11011 = 10011
```

##### Qu'est ce que fais le bitwise XOR ?

Il met a 1 tous les valeur dont l'indice d'une des deux chaine comparer est a 1 met seulemennt 1 les autre il l'est met a 0.

##### Quelle est le symbol de l'operation xor ?

`^=`

##### Comment faire pour flipper un bit de valeur avec le bitwise XOR operateur ?

Si on veu flipper le bit a l'index 2 

```
A = 10101
B = 1 << 2 // 00100
A ^= B     // 10101 ^= 00100 = 10001
```

##### Pourquoi est ce que lorsque emprunnte un 1 sur la colonne de gauche lors d'une soustraction binaire on emprunte 2 ?

pcqu'ont est en base 2 comme en base 10 on emprunte 10.

##### Commennt faire pour initialiser un set de n element a 1 ?

Il faut shifert du chiffre binaire 1 n case puis lui soustraire -1.

exemple : si on changer tous les bit d'un set de 4 element a 1

```
t = 1 << 4 // 10000
t -= 1     //  1111 car 10000 - 00001 = 1111
```

##### Comment connvertir un nombre en son complement 2 ? (positif -> negatif / negatif -> positif) ?

Il faut flipper les bits puis ajouter 1 au nombre.

##### Comment faire pour retourenr la chaine binaire du dernier bit qui a une valeur de 1 ?

```
(s & (-s))
``` 

# ex 2.2.2

##### Quel est l'operation binaire que lont doit faire pour obtenir le modulo d'un nombre diviser par unne puissance de 2 ?

Ont fait -1 a la puissance de 2 puis apres on effectur unn Bitwise AND avec le nombre sur lequel on veu effectuer le modulo.

si `n = le nombre surlequel on veu effectuer le modulo` et `x = le modulo`

`(n & (x-1))`

##### Qu'est ce que les nombre binaire qui sont aussi des puissances de 2 ont en commun ?

Leur seul bit qui vaut 1 est le bit le plus a gauche.

`(x & (x - 1))`

# C++ map

##### Comment instancier un objet map ?

```
std::map<char,int> map;
```

##### Comment instancier un iterator ?

```
std::iterator<char,int> it;
```

##### Quelle sont les deux maniere d'inserer des donnees dans un map ?

Avec les operateur [] ou la methode map.insert().

##### Quelle est la difference entre ces deux methode d'insertion ?

avec les [] si la valeur existe deja elle sera changer.
avec la methode insert retournne un boolean qui est false si la cle est deja presente.

##### Comment utilise la methode map.insert() ?

```c++
std::map<string,string> mymap;
mymap.insert(std::make_pair("Jordan","Gauthier"))
```

##### Comment iterer a travers toute une map ?

Il faut utiliser un iterator.

```c++
std::map<string,string> names;
std::map<string,string>::iterator it;

names.insert(std::make_pair("Jordan","Gauthier"));
names.insert(std::make_pair("Charles-olvier","Gauthier"));

it = names.begin();

while(it != names.end()){
    cout << it->first << " " << it->second << std::endl;
}

```
##### Comment faire pour recercher un element par sa cle dans une map ?

Avec la methode find de l'objet map.

```c++
std::map<string,string> names;
std::map<string,string>::iterator it;

names.insert(std::make_pair("Jordan","Gauthier"));
names.insert(std::make_pair("Charles-olvier","Gauthier"));

if(names.find("jordan") != names.end())
    cout << "Jordan est la" << endl;
else
    cout << "Pas trouver" << endl;

```

##### Qu'est ce que fais l'operateur ++ avec un iterateur (e.g : it++) ?

Il  pointe sur le prochain objet de l'iterateur

# Problem solved

##### Comment simuler une linkedlist doublement chaine avec des tableau ?

tableau1 : c'est le node de la valeur principal
tableau2 : Chacun de ses indice i correspond a la position du voisin de gauche de la case tableau1[i]. 
tableau3 : Enregistre pour chaque indice i du tableau 1 la position j du voisin de droite le plus proche.

Si on prend par exemple un node sois tableau1[5] son node.left et node.right seront respectivement :

node.left = tableau1[tableau2[5]];
node.right = tableau1[tableau3[5]];

##### Quelle est la formule pour savoir la valeur maximal d'un nombre avec un nombre de bit donner (unsigned et signed) ?

Si n est le nombre de bit :

signed   : `(int)(((2^n) - 1) / 2)`
unsigned : `(2^n) - 1`
`(1 << n) - 1`

# Theorem

##### Quelle est la formule pour trouver le prochainn nombre ayant unne racine carrer entier a partir d'un nombre N ayant aussi une racine carrer entiere ?

`N + ( sqrt(N) * 2 ) + 1`

Exemple :

sqrt(9) = 3.. -> 9 + ( sqrt(9) * 2 ) + 1 = 9 + ( 3 * 2 ) + 1 = 9 + 6 + 1 = 16

##### Quelle est la formule pour trouver le nombre de nombre entre un nombre N ayant unen racinne carrer entiere et le prochain nombre ayannt une racine carrer entiere ?

`sqrt(N) * 2`

Exemple :

3^2 = 9 4^2 = 16 

le nombre de nombre entre ces deux nombre = sqrt(9) * 2 = 3 * 2 = 6

##### Quelle est la formule pour trouver la racine carrer entiere la plus proche d'un nombre M ayant une racine carrer decimal ?

`round ( sqrt ( M ) )`

Exemple :

sqrt(7) = 2.6...

round( sqrt( 7 ) ) = round( 2.6.. ) = 3
 
##### Quelle est la formule pour trouver le Nombre N le plus proche ayant une racinne carrer entiere d'un nombre M ayant unne racine carrer decimal ?

`pow( round( sqrt( M ) ), 2 )`

Si on continue l'exemple avec l'exemple de la question precedente :

round( sqrt( 7 ) ) = round( 2.6.. ) = 3

Alors :

pow( round( sqrt( 7 ) ), 2) = round( 2.6.. ) = pow( round( 2.6.. ), 2) = pow( 3, 2 ) = 9

##### Quelle est le truc pour trouver le nombreb maximal que POURRAIT etre le plus petit facteur d'une pair de nombre a et b telle que a * b = n ou a <= b ?

Il faut faire la racine carrer du nombre.

Exemple :

N = 36

sqrt(36) = 6

36 / 1 = 36
36 / 2 = 18
36 / 3 = 12
36 / 4 = 9
36 / 5 = pas de nb entier
36 / 6 = 6

SI ON CONTINUE NOUS ALLONS VOIR QUE LE PATTERN SE REPETE EN INVERSANT LES NOMBRES.

36 / 7 = pas de nb entier
36 / 8 = pas de nb entier
36 / 9 = 4 (36 / 4 = 9 existe deja)

##### Que peut ont dire de sqrt(n)^2 par rapport a la question precedente ?

Que sqrt(n)^2 est la pair de nombre du milieu qui separe la repetition du pattern.
il est aussi le pair de nombre pour laquelle la somme est la plus petite pour laquelle le produit
est egale a sqrt(n)^2.

##### Comment obtenir un nombre binaire composer uniquement de 1 ?

il faut faire (2^n) - 1 ou n est le nombre de bit dans notre nombre binaire voulu.

Exemple si on veu le nombre 1111 :

(2^4) - 1 = 16 - 1 = 15 = 1111

##### Quel est le truc avec un bitset pour comparer des sequennce de bit a l'interieur du bitset sans qu'une sequence ne s'overlap sachant que les interval sont a extremiter ouverte ?

On set le premier bit a l'avant dernier bit en testant chacun des bit. Comme ca a chaque nouvelle sequence le dernier bit se sera pas pris en compte et le test fonctionnera
dans les deux cas sois si la nouvelle sequence commennce sur ce dernier bit non setter ou si une nouvelle sequence commennce sur ce bit non setter.

##### Quel est l'implementation en arriere de la std::map et std::set ?

C'est un balannced binary tree (arbre en ordre comme dans le binary search)

# Heap

##### Quelle sont les deux sorte de binary heap tree ?

min et max

##### A quelle index commecne un binary heap tree ?

A 1 pas a 0

##### Quelles sont les 2 formule pour obtenir le left et right child d'un node dans un binary heap tree ?

left child = nodeIndex * 2 
right child = nodeIndex * 2 + 1

##### Quelle est le concept des min et max heap tree ?

Min : le root node est tous le temps plus petit que ses enfants
Max : le root node est tous le temps plus grand que ses enfants

##### Comment inserer un element dans un heap tree ?

Min :

On innserer tous le temps le node a la prochaine place dasn le tree en partant de la gauche. S'il n'est pas la bonne place on le bubble swap avec son parent
jusqu'attend qu'il atteigne l'endroit approprier.

Max : 

On insere le node a la prochaine place en partant de la gauche. S'il n'est pas a la bonne place on le bubble up avec sont parent jusqu'attend qu'il le soit.

##### Comment supprimer le root node du heap tree ?

on swap le dernier element ajouter au tree a la root puis on le bubble down jusqu'attend qu'il sois a sa place.

##### Comment vaut-il mieux representer unn heap tree ?

Avec un simple tableau

##### Qu'est u'un complete tree ?

Un arbre dont tous les niveau somt remplie excepter peut etre le dernier

##### Qu'est ce que la max heap property ?

la valeur du node est plus grande ou egal a la valeur de ses enfants.

##### Qu'est ce que le min heap property ?

le contraire du max heap

##### pourquoi est ce que la fonction max_heapify part de n / 2 ?

pcq c'est le dernier node qui a des enfants.

##### Qu'est ce que fais la fonction max_heapify ?

Elle va reparer une violation du heap.

##### Qu'est ce que la data structure priority queue est en c++ ?

un max heap

##### Quel est la difference entre un set et un multiset ?

un set ne peu pas avoir la meme valeur plus d'une fois
un multiset peu avoir la meme valeur plusieurs fois.
