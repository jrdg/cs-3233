/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include "Noeud.h"

class Avl_tree;
class Avl_tree{
	private:
		Noeud * racine;
	public:

		/**
			Instancie un arbre avl completement vide.
		*/
		Avl_tree( void );
		
		/**
			Instancie un Avl_tree avec un noeud de depart

			@param noeud Le noeud de depart de l'avl_tree.
		*/
		Avl_tree( Noeud & noeud );

		/**
			Ajoute un nœud dans l’arbre et le
			rééquilibre. Devra aussi maintenir l’intégrité des a. Utilisé pour 
			la commande insertion
			
			@param clef La valeur de clef
			@param valeur La valeur de noeud
		*/
		void inserer( double clef, double valeur );

		/**
			Retourne une liste de tous les pair cle valeur ayant 
			une cle <= a x

			@param x La valeur a comparer avec les cles

			@return  Un vecteur de pair
		*/		
		std::vector< std::pair< double, double > > jusqua( double x );
		
		/**
			Inserer les pair cle valeur dans liste ou les cle son <= x

			@param liste Le vecteur dans lequel les pair cle valeur seront 
				   inserer
			@param noeud Une reference sur un pointeur de de type Noeud
			@param x La valeur sur laquel les cle seront compare.
		*/
		void ajouter_liste( std::vector< std::pair< double, double > > & liste, 
													Noeud *& noeud, double x );

	
		/**
			Insere un noeud dans la structure avec comme valeur de cle : cle et 
			valeur : valeur

			@param noeud le noeud sur lequel l'appel recursif fait son action
			@param clef la valeur de la clef
			@param valeur la valeur de la valeur

			@return vrai si equilibre == 1 ou -1 ou si le noeud a 
					ete inserer ou si le noeud est deja dans l'arbre
		*/	
		bool inserer( Noeud *& noeud, double clef, double valeur );

		/**
			Retourne la valeur de a placé à la racine. Utilisé pour répondre à 
			la commande max.
			
			@param clef La valeur de clef rechercher pour renvoyer la valeur de 
				   son a.
		*/
		double maxima( void );

		/**
			retourne la valeur a du noeud ayant comme valeur de clef : x.

			@param x valeur de la clef
		*/
		double maxima( double x );

		/**
			Trouve la valeur a du noeud ayant comme valuer de clef : x

			@param node le noeud sur lequel la clef est presentement verifier
			@param x clef rechercher
		*/
		double trouver_max( Noeud *& node, double x );
	
		/**
			Retourne la valeur maximale des valeurs associées aux clefs
			plus petites ou égales à x. Utilisé pour la commande donne et 
			appartient.

			@param x La valeur de comparaison des clefs.

			@return un double.
		*/		
		double aGauche( double x );

		/**
			Retourne la valeur maximal des valeurs associees au clefs
			plus petites ou egale a x. Utilise pour la commande donne et 
			appartient.

			@param noeud le noeud presentement iterer
			@param x La valeur a comparer

			@return Un double.
		*/
		double aGauche( Noeud *& noeud, double x );

		/**
			Acces a la tete de l'arbre avl
			
			@return Une reference sur le pointeur pointant 
				    sur le noeud en guise de tete de l'arbre avl.
		*/
		Noeud *& get_head( void );

		/**
			Effectue une rotation de gauche a droite pour reequilibre l'arbre 
			avl.

			@param racinesousarbre Le noeud necessistant un reequilibrage.
		*/
		void rotationGaucheDroite( Noeud *& racinesousarbre );

		/**
			Effectue une rotation de droite a gauche pour reequilibre l'arbre 
			avl.

			@param racinesousarbre Le noeud necessistant un reequilibrage.
		*/
		void rotationDroiteGauche( Noeud *& racinesousarbre );
};

