/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include "Avl_tree.h"

#define ui unsigned int
#define ull unsigned long long
#define ll long long
#define st size_t
#define us unsigned short
#define asum(f,l,n) ((n*(f+l))/2)
#define gsum(f,r,n) ((f*(1-pow(r,n)))/(1-r))
#define gn(f,n,r) (f*pow(r,n-1))
#define an(f,n,r) (f+r*(n-1))
#define COMMANDE_INSERTION 0
#define COMMANDE_APPARTIENT 1
#define COMMANDE_DONNE 2
#define COMMANDE_AVANT 3
#define COMMANDE_MAX 4


/**
	Compare deux chaine de caractere

	@param str1 la premiere chaine
	@param str2 la deuxieme chaine

	@return 0 si les deux chaine ne sont pas egale
*/
unsigned int strc(
	char * str1,
	char * str2
) {
	st i1,i2, i;
	i1 = i2 = i = 0;
	unsigned int is_equal = 1;
	while(str1[i1] != '\0' && str1[i1] != '\n') i1++;
	while(str2[i2] != '\0' && str2[i2] != '\n') i2++;
	if(i1 == i2){
		while(i < i1 && str1[i] == str2[i]) i++;
		is_equal = i == i1 ? i : 0;
	}else{
		is_equal = 0;
	}
	return is_equal;
}

unsigned int reset_buffer(
	char buffer[]
) {
	buffer[0] = '\0';
	return 1;
}

/**
	cette fonction test si le contenue de buffer est compatible avec le format
	de la commande insertion.
	
	@param buffer		Le buffer contenant la chaine qui doit correspondre au 
				        format de la commande 
	@param empty_buffer Le buffer qui sert a regarder si il reste du texte apres 
						les caracteres d'une commande valide.
	@param c1 			Le premier caractere du pattern a matcher
	@param c2			Le deuxieme caractere du pattern a matcher
	@param c3			Le troisieme caractere du pattern a matcher
	@param c4			Le quatrieme caractere du pattern a matcher
	@param cle			La valeur de la cle de l'intervale
	@param valeur		La valeur de la valeur de l'intervale.

	@return vrai si le format correspond a la commande insertion.
*/
unsigned int is_insertion(
	char * buffer,
	char * empty_buffer,
	char & c1,
	char & c2,
	char & c3,
	char & c4,
	double & cle,
	double & valeur
) {
	return reset_buffer(empty_buffer) && 
		   sscanf(buffer, " %c %lf %c %lf %c %c %s ",
		   &c1, &cle, &c2, &valeur, &c3, &c4, empty_buffer) &&
           c1 == '(' && c2 == ',' && c3 == ')' && c4 == '.' 
           && strlen(empty_buffer) == 0;
}

/**
	cette fonction test si le contenue de buffer est compatible avec le format
	de la commande max.
	
	@param buffer		Le buffer contenant la chaine qui doit correspondre au 
						format de la commande 
	@param empty_buffer Le buffer qui sert a regarder si il reste du texte apres 
						les caracteres d'une commande valide.
	@param c1 			Le premier caractere du pattern a matcher
	@param c2			Le deuxieme caractere du pattern a matcher
	@param c3			Le troisieme caractere du pattern a matcher
	@param c4			Le quatrieme caractere du pattern a matcher

	@return vrai si le format correspond a la commande max.
*/
unsigned int is_max(
	char * buffer,
	char * empty_buffer,
	char & c1,
	char & c2,
	char & c3,
	char & c4
) {
	return reset_buffer(empty_buffer) && 
		   sscanf(buffer, " %c %c %c %c %s ", 
           &c1, &c2 , &c3, &c4, empty_buffer) && c1 == 'm' 
           && c2 == 'a' && c3 == 'x' && c4 == '?' && 
           strlen(empty_buffer) == 0;
}

/**
	cette fonction test si le contenue de buffer est compatible avec le format
	de la commande appartient.
	
	@param buffer		Le buffer contenant la chaine qui doit correspondre au 
						format de la commande 
	@param empty_buffer Le buffer qui sert a regarder si il reste du texte apres 
						les caracteres d'une commande valide.
	@param c1 			Le premier caractere du pattern a matcher
	@param x			La valeur supposer etre include dans un des intervales.

	@return vrai si le format correspond a la commande appartient.
*/
unsigned int is_appartient(
	char * buffer,
	char * empty_buffer,
	char & c1,
	double & x
) {
	return reset_buffer(empty_buffer) && 
		   sscanf(buffer, " %lf %c %s ", &x, &c1, empty_buffer) 
		   && c1 == '?' && strlen(empty_buffer) == 0;
}

/**
	cette fonction test si le contenue de buffer est compatible avec le format
	de la commande donne.
	
	@param buffer		Le buffer contenant la chaine qui doit correspondre au 
						format de la commande 
	@param empty_buffer Le buffer qui sert a regarder si il reste du texte apres 
						les caracteres d'une commande valide.
	@param c1 			Le premier caractere du pattern a matcher
	@param c2			Le deuxieme caractere du pattern a matcher
	@param c3			Le troisieme caractere du pattern a matcher
	@param c4			Le quatrieme caractere du pattern a matcher
	@param c5			Le cinquieme caractere du pattern a matcher
	@param c6			Le sixieme caractere du pattern a matcher
	@param cle			La valeur de la cle de l'intervale

	@return vrai si le format correspond a la commande donne.
*/
unsigned int is_donne(
	char * buffer,
	char * empty_buffer,
	char & c1,
	char & c2,
	char & c3,
	char & c4,
	char & c5,
	char & c6,
	double & cle
) {
	return	reset_buffer(empty_buffer) && 
			sscanf(buffer," %c %c %c %c %c %lf %c %s ", 
			&c1, &c2, &c3, &c4, &c5, &cle, &c6, empty_buffer) && 
			c1 == 'd' && c2 == 'o' &&
            c3 == 'n' && c4 == 'n' &&
            c5 == 'e' && c6 == '?' && 
			strlen(empty_buffer) == 0;
}

/**
	cette fonction test si le contenue de buffer est compatible avec le format
	de la commande avant.
	
	@param buffer		Le buffer contenant la chaine qui doit correspondre au 
						format de la commande 
	@param empty_buffer Le buffer qui sert a regarder si il reste du texte apres 
						les caracteres d'une commande valide.
	@param c1 			Le premier caractere du pattern a matcher
	@param c2			Le deuxieme caractere du pattern a matcher
	@param c3			Le troisieme caractere du pattern a matcher
	@param c4			Le quatrieme caractere du pattern a matcher
	@param c5			Le cinquieme caractere du pattern a matcher
	@param c6			Le sixieme caractere du pattern a matcher
	@param cle			La valeur de la cle de l'intervale

	@return vrai si le format correspond a la commande avant.
*/
unsigned int is_avant(
	char * buffer,
	char * empty_buffer,
	char & c1,
	char & c2,
	char & c3,
	char & c4,
	char & c5,
	char & c6,
	double & cle
) {
	return reset_buffer(empty_buffer) &&
		   sscanf(buffer," %c %c %c %c %c %lf %c %s ", 
		   &c1, &c2, &c3, &c4, &c5, &cle, &c6, empty_buffer) &&
		   c1 == 'a' && c2 == 'v' &&
		   c3 == 'a' && c4 == 'n' &&
		   c5 == 't' && c6 == '?' &&
		   strlen(empty_buffer) == 0;
}

/**
	Execute les actions de la commande avant
	
	@param tree La structure AVL contenant les intervalles.
	@param x La valeur comparer au reste des cles
*/
void execute_avant(
	Avl_tree & tree,
	double & x	
) {
	std::vector< std::pair< double, double > > liste = tree.jusqua(x);
	std::cout << "[";
	if(liste.size() > 0) {
		std::cout << "(" 
				  << liste.at(0).first 
			      << "," 
				  << liste.at(0).second << ")";
		for(int i = 1 ; i < liste.size() ; i++){
			std::cout << "," 
					  << "(" << liste.at(i).first 
					  << "," 
					  << liste.at(i).second << ")";
		}
	}
	std::cout << "]" << std::endl;
}

int main (){
	char buffer[10000], empty_buffer[10000];
	unsigned int no_error = 1;
	double n1, n2;
	Avl_tree tree;
	char c1, c2, c3, c4, c5, c6;
	while(no_error && 
		  fgets(buffer, 10000, stdin) != NULL && !strc(buffer, "q.")){
		if(is_insertion(buffer, empty_buffer, c1, c2, c3, c4, n1, n2)) {
			tree.inserer(n1, n2);
		} else if (is_max(buffer, empty_buffer, c1, c2, c3, c4)) {
			std::cout << tree.maxima() << std::endl;
		} else if (is_appartient(buffer, empty_buffer, c1, n1)) {
			std::cout << ( n1 <= tree.aGauche(n1) ? "vrai" : "faux" ) 
					  << std::endl;
		} else if (is_donne(buffer, empty_buffer, c1, c2, c3, c4, c5, c6, n1)) {
			std::cout << tree.aGauche(n1) << std::endl;
		} else if (is_avant(buffer, empty_buffer, c1, c2, c3, c4, c5, c6, n1)) {
			execute_avant(tree, n1);
		} else{
			std::cerr << "ERROR : TERMINATED" << std::endl;
			no_error = 0;
		}
		buffer[0] = '\0';
		c1 = c2 = c3 = c4 = c5 = c6 = '0';
	}
	return 0;
}

