/**
	@Nom Jordan Gauthier
	@code_permanent gauj25089201
*/

#include "Noeud.h"

/**
	Instancie un Noeud avec une clef et une valeur.
	Le A = valeur et l'equilibre = 0.

	@param clef la valeur de la clef
	@param valeur la valeur de la valeur
*/
Noeud::Noeud(
	double clef, 
	double valeur
) {
	this->clef = clef;
	this->valeur = valeur;
	gauche = NULL;
	droite = NULL;
	a = valeur;
	equilibre = 0;
}
		
/**
	Acces au noeud de gauche du noeud courrant.
	
	@return Une reference sur le pointeur du noeud de gauche.
*/
Noeud *& Noeud::get_gauche(
	void
) {
	return this->gauche;
}

/**
	Acces au noeud de droite du noeud courrant.
	
	@return Une reference sur le pointeur du noeud de droite.
*/
Noeud *& Noeud::get_droite(
	void
) {
	return this->droite;
}

/**
	Acces a la valeur du A de ce noeud.

	@return Un double representant le A.
*/
double Noeud::get_a(
	void
) {
	return this->a;
}

/**
	Acces a la valeur de ce noeud

	@return un double representant la valeur de ce noeud.
*/
double Noeud::get_valeur(
	void
) {
	return this->valeur;
}

/**
	Acces a la valeur de la clef de ce noeud.

	@return un double representant la valeur de la clef.
*/
double Noeud::get_clef(
	void
) {
	return this->clef;
}

/**
	Acces a la valeur de l'equilibre de ce noeud.

	@return un entier representant la valeur de l'equilibre de ce noeud.
*/
int Noeud::get_equilibre(
	void
) {
	return this->equilibre;
}

/**
	Modifie la valeur du A de ce noeud

	@param a la nouvelle valeur que prendra le a			
*/
void Noeud::set_a(
	double a
) {
	this->a = a;
}

/**
	modifie la valeur de la valeur du noeud.
	
	@param valeur la nouvelle valeur
*/
void Noeud::set_valeur(
	double valeur
) {
	this->valeur = valeur;
}

/**
	modifie la valeur de la clef

	@param clef la nouvelle valeur de la clef 
*/
void Noeud::set_clef(
	double clef
) {
	this->clef = clef;
}

/**
	Modifie le noeud de gauche

	@param noeud le nouveau noeud de gauche
*/
void Noeud::set_gauche(
	Noeud *& noeud
) {
	this->gauche = noeud;
}

/**
	Modifie le noeud de droite

	@param noeud le nouveau noeud de droite
*/
void Noeud::set_droite(
	Noeud *& noeud
) {
	this->droite = noeud;
}

/**
	Modifie l'equilibre du noeud

	@param equilibre la nouvelle equilibre du noeud.
*/
void Noeud::set_equilibre(
	int equilibre
) {
	this->equilibre = equilibre;
}
